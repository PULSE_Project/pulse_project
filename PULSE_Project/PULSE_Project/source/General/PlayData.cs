//--------------------------------------------------------------
// クラス名： PlayData.cs
// コメント： プレイデータ管理クラス
// 製作者　： MasayukiTada
// 制作日時： 2014/03/03
//--------------------------------------------------------------
using System;

namespace PULSE_Project
{
	public class PlayData
	{
		private static PlayData singleton;	// 唯一のインスタンス
		
		public float remainingLife;						// 残りライフ
		public int defeatedEnemyNum;					// 倒した敵の数
		
		public int comboMax{get; private set;}			// コンボの最大数
		public int notePerfectNum{get; private set;}	// ノートのパーフェクトの数
		public int noteGoodNum{get; private set;}		// ノートのグッドの数
		public int noteBadNum{get; private set;}		// ノートのバッドの数
		public int noteMissNum{get; private set;}		// ノートのミスの数
		
		public NotesData notesData;						// 遊んだ楽曲情報
		
		// コンストラクタ
		private PlayData ()
		{
			Init ();
		}
		
		// 初期化
		public int Init()
		{
			comboMax = 0;
			remainingLife = 0;
			defeatedEnemyNum = 0;
			notePerfectNum = 0;
			noteGoodNum = 0;
			noteBadNum = 0;
			noteMissNum = 0;
			
			notesData = new NotesData();
			
			return 0;
		}
		
		// ノートのランクを受け取って判別して加算
		public void AddNoteRank(TimingRank noteRnak)
		{
			switch(noteRnak)
			{
			case TimingRank.PERFECT:
				notePerfectNum++;
				break;
			case TimingRank.GOOD:
				noteGoodNum++;
				break;
			case TimingRank.BAD:
				noteBadNum++;
				break;
			case TimingRank.MISS:
				noteMissNum++;
				break;
			case TimingRank.NONE:
				break;
			}
		}
		
		// コンボ数を受け取って最大かどうか確認(最大なら上書き)
		public void MaxComboCheck(int combo)
		{
			if(combo > comboMax) comboMax = combo;
		}
		
		public static PlayData GetInstance()
		{
			if(singleton == null) singleton = new PlayData();
			return singleton;
		}
	}
}

