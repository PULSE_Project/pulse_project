//--------------------------------------------------------------
// クラス名： PlayersUnit.cs
// コメント： ゲーム画面中央のユニット制御クラス. プレイヤーの操作の要となる.
// 製作者　： ShionKubota
// 制作日時： 2014/03/02
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

namespace PULSE_Project
{
	public class PlayersUnit
	{
		public const float RADIUS  = 80.0f;	// ユニットの半径
		public const float SCALE = 0.2f;		// ユニット画像スケール
		public const float LIFE_MAX = 10.0f;	// ライフ最大値
		
		private const long PERFECT_ZONE = 150L;		// ボタン押下とノートのタイミング評価基準
		private const long GOOD_ZONE    = 550L;
		private const long BAD_ZONE     = 700L;
		
		private const float LIFE_COLOR = 1.0f*2;	//ライフカラーの使用色とその段階数値
		private const float LIFE_COLOR_VOL = LIFE_COLOR / LIFE_MAX;	//ライフカラーの1ライフ分の増減量
		
		// 円形ユニットの画像
		private Sprite2D unitSp;
		private Texture2D unitTex;
		
		//　ユニット周りのリング
		private Sprite2D unitRingSp;
		private Texture2D unitRingTex;
		private float	prevLife;
		
		// 照準線の画像
		private Sprite2D lineSp;
		private Texture2D lineTex;
		private float shotAngle;
		
		// 押下時のSE
		private MusicEffect pushSe;
		
		//ShotManager
		private ShotManager ShotManager;
		
		// 各種情報
		public float life{ get; private set; }	// プレイヤーの体力
		public int combo{ get; private set; }	// コンボ数
		private bool longNoteNow;				// 長押し状態か否か
		
		public PlayersUnit ()
		{
			Init ();
		}
		
		// 初期化
		private void Init()
		{
			// ユニット画像
			unitTex = new Texture2D(@"/Application/assets/img/CentralUnit.png", false);
			unitSp = new Sprite2D(unitTex);
			unitSp.pos = AppMain.ScreenCenter;
			unitSp.color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
			unitSp.size = new Vector2(SCALE, SCALE);
			
			//　中央ユニット周りのリング
			unitRingTex 	= new Texture2D(@"/Application/assets/img/PulseRing.png", false);
			unitRingSp 		= new Sprite2D(unitRingTex);
			unitRingSp.pos 	= AppMain.ScreenCenter;
			unitRingSp.color = new Vector4(0.0f, 1.0f, 0.0f, 1.0f);
			unitRingSp.size = new Vector2(SCALE, SCALE);
			prevLife		= LIFE_MAX;
			
			// 照準ライン画像
			lineTex = new Texture2D(@"/Application/assets/img/SightLine.png", false);
			lineSp = new Sprite2D(lineTex);
			lineSp.pos = AppMain.ScreenCenter;
			lineSp.size = new Vector2(2.0f, 2.0f);
			lineSp.color = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
			
			// SE
			pushSe = new MusicEffect(@"/Application/assets/se/button55.wav");
			
			shotAngle = 0.0f;
			life = LIFE_MAX;
			combo = 0;
			
			//ShotManager
			ShotManager = new ShotManager();
		}
		
		// 更新
		public void Update()
		{
			Input input = Input.GetInstance();
			
			ManageInput(input);
			ManageLine(input);
			ManageLifeColor();
			
			//ShotManager
			ShotManager.Update();
		}
		
		// 入力(ノート押し、ショット)管理-----------------------------------------
		private void ManageInput(Input input)
		{
			// 通常ノートの押下チェック
			if(!longNoteNow)
			{
				if(input.circle.isPushStart)	// 押した瞬間
				{
					CheckTimingAndShot();
				}
			}
			// ロングノート中のチェック
			else
			{
				ShotManager.LongShot(shotAngle, TimingRank.PERFECT, combo);
				
				if(input.circle.isPushEnd)
				{
					CheckTimingAndShot();
				}
			}
			
			// ボタンが離されたらロングノート状態を解除
			if(Input.GetInstance().circle.isPushEnd && longNoteNow)
			{
				BreakLongNote();
			}
		}
		
		// 呼ばれたタイミングを確認し、ノートのタイミングと合っていたらショットを放つ。
		private void CheckTimingAndShot()
		{
			long nowTime = GameScene.noteManager.stopWatch.ElapsedMilliseconds;
			TimingRank rank = TimingRank.NONE;
			
			// 各ランクの許容時間テーブル
			long[] timingRankTable = new long[]{PERFECT_ZONE, GOOD_ZONE, BAD_ZONE};
			
			// リングの中から、タイミングに合うものがあるかどうか探す
			foreach(PulseRing ring in GameScene.noteManager.pulseRings)
			{
				// タイミングテーブルを回し、適合したらショット
				for(int i = 0; i < timingRankTable.Length; i++)
				{
					// タイミング差が許容時間を超えていたらcontinueでテーブルを回す
					if(Math.Abs(ring.note.timing - nowTime) >= timingRankTable[i]) continue;	
					
					// テーブル中のいずれかに該当したらランクが決定.
					rank = (TimingRank)i;
					ring.note.isClear = true;
					longNoteNow = ring.note.type == Note.Type.START ? true : false;	// ロングノートモードのon/off切り替え
					combo = (rank != TimingRank.BAD) ? combo + 1 : 0;	// コンボ数をカウント or リセット
					PlayData.GetInstance().MaxComboCheck(combo);		// コンボ数を集計
					PlayData.GetInstance().AddNoteRank(rank);			// タイミングのランクを集計
					GameScene.gameUI.DispTimingRank(rank);				// タイミングの評価を演出として表示させる
					pushSe.Set ();	// SE再生
					
					// 通常ノートならショット
					if(ring.note.type == Note.Type.NORMAL)
						ShotManager.Shot(shotAngle, rank, combo);
					return;
				}
			}
		}
		
		// 照準のライン管理----------------------------------------------------------
		private void ManageLine(Input input)
		{
			// アナログパッド入力が(ほぼ)無いとき、ラインをフェードアウト
			const float THRESHOLD = 0.4f;	// アナログパッド入力しきい値
			if(Math.Abs(input.GetAnalogL().X) < THRESHOLD &&
			   Math.Abs(input.GetAnalogL().Y) < THRESHOLD)
			{
				if(lineSp.color.W > 0)
					lineSp.color.W -= 0.1f;;
				return;
			}
			// 入力があればフェードイン
			else
			{
				if(lineSp.color.W < 1.0f)
				lineSp.color.W += 0.1f;
			}
			
			// アナログパッドによる照準ラインの回転
			shotAngle = FMath.Atan2(input.GetAnalogL().Y, input.GetAnalogL().X);
			lineSp.pos.X = AppMain.ScreenCenter.X + FMath.Cos(shotAngle) * (lineSp.width / 2);
			lineSp.pos.Y = AppMain.ScreenCenter.Y + FMath.Sin(shotAngle) * (lineSp.width / 2);
			lineSp.angle = FMath.Degrees(shotAngle);
		}
		
		// ライフ表示管理
		private void ManageLifeColor()
		{
			//ライフカラー処理
			if(prevLife - life > 0.001f)
			{
				prevLife -= (prevLife - life) / 5.0f;
			}
			//1フレーム前の変数でグラデーション化
			unitRingSp.color.R = 2.0f - (prevLife * LIFE_COLOR_VOL);
			unitRingSp.color.G = prevLife * LIFE_COLOR_VOL;
			
			if(unitRingSp.color.R > 1.0f) unitRingSp.color.R = 1.0f;
			if(unitRingSp.color.G > 1.0f) unitRingSp.color.G = 1.0f;
		}
		
		// ロングノート状態を解除
		public void QuitLongNote()
		{
			longNoteNow = false;
		}
		
		// ロングノート状態を解除して終端ノートを削除
		public void BreakLongNote()
		{
			longNoteNow = false;
			// ロングノートの終わりを検出して消去
			for(int i = 0; i < GameScene.noteManager.notesData.notes.Count - 1; i++)
			{
				Note note = GameScene.noteManager.notesData.notes[i];
				if(!note.isClear && note.type == Note.Type.END)
				{
					note.isClear = true;
					break;
				}
			}
		}
		
		// 描画
		public void Draw()
		{
			if(!longNoteNow) lineSp.Draw();
			unitSp.Draw();
			unitRingSp.Draw ();
			//ShotManager
			ShotManager.Draw();
		}
		
		// 解放
		public void Term()
		{
			unitTex.Dispose();
			lineTex.Dispose();
			unitRingTex.Dispose();
			pushSe.Term();
			
			//ShotManager
			ShotManager.Term();
		}
		
		// ダメージを受ける
		public void AddDamage(float damage)
		{
			damage = damage < 0.0f ? 0.0f : damage;	// マイナスのダメージを受け付けない
			
			prevLife = life;
			
			life -= damage;
		}
		
		// コンボ数を0に戻す
		public void ResetCombo()
		{
			combo = 0;
		}
	}
}

