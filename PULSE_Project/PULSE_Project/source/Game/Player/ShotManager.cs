using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

namespace PULSE_Project
{
	public class ShotManager
	{
		//定数
		
		//辺り範囲(角度)ノーマルショット:N　,ロングショット:L
		private const float N_RANGE = 20.0f;				
		private const float L_RANGE = 25.0f;
		
		//ダメージ値	ノーマルショット:N　,ロングショット:L
		private const float N_DAMAGE = 5.0f;				
		private const float L_DAMAGE = 0.2f;				
				
		//レーザーテクスチャのサイズ
		private Vector2 SHOT_TEX_SIZE  = new Vector2(512.0f,64.0f);
		private Vector2 SHOT_TEX_CENTER= new Vector2(0.0f,0.0f);
		
		//レーザーの最大表示数
		private const int SHOTMAX		= 5;
		
		
		//レーザーのスプライトとそのフラグ
		private	Sprite2D[] 	shotSp 	= new Sprite2D[SHOTMAX];
		private bool[]		shotFlg	= new bool[SHOTMAX];
		
		//レーザーテクスチャ
		private Texture2D	shotTex	;
		/*
		private Texture2D[]	shotTex		= new Texture2D[SHOTMAX];
		
		//レーザーテクスチャ用列挙方
		enum TexNum
		{
			NORMAL 	= 0,
			SHOT	= 1,
			NEWSYOT	= 2
		}
		*/
		
		//角度の取得
		private Vector2 shotAngleVec;
		
		//ロングノート用
		private	Sprite2D	longShotSp;
		private bool		longShotFlg;
		private bool		playerTouchFlg;
		private float			colorCnt;
		
		//ダメージ計算用の変数保持
		private TimingRank 	saveRank;
		private int 		saveCombo;
		private float		saveAngle;
			
		
		//コンストラクト
		public ShotManager ()
		{
			Init ();
		}
		
		//初期化
		private void Init()
		{
			//定数用
			//SHOT_TEX_CENTER= new Vector2(SHOT_TEX_SIZE.X / 2.0f,SHOT_TEX_SIZE.Y / 2.0f);
			SHOT_TEX_CENTER= new Vector2(1.0f, 1.0f);
			
			//テクスチャおよびスプライトの初期化
			shotTex = new Texture2D (@"/Application/assets/img/SightLine.png", false);
			for (int i=0 ; i<SHOTMAX ; i++)
			{
				//shotSp[i] = NULL;
				shotFlg[i]= false;
			}
			
			shotAngleVec = new Vector2(0.0f, 0.0f);
			
			//ロングノート用
			longShotSp	= new Sprite2D(shotTex);
			longShotFlg	= false;
			playerTouchFlg	= false;
			colorCnt 	= 0;
			
			//保持用初期化
			saveRank = 0;
			saveCombo= 0;
			saveAngle= 0;
			
		}
		
		//更新
		public void Update()
		{
			//デバック用：4方向出現
			/*
			if(Input.GetInstance().circle.isPushStart == true)	LongShot(0.0f,TimingRank.BAD,0);
			if(Input.GetInstance().cross.isPushEnd == true)	LongShot(90.0f,TimingRank.BAD,0);
			if(Input.GetInstance().square.isPush == true)	LongShot(180.0f,TimingRank.BAD,0);
			if(Input.GetInstance().triangle.isPushStart == true)LongShot(270.0f,TimingRank.BAD,0);
			*/
			
			//ノーマルショット更新
			for (int i=0 ; i<SHOTMAX ; i++)
			{
				if(shotFlg[i] == false) continue;
				
				//デバック用：回転
				//shotSp[i].color.W -=0.001f;
				//if(shotSp[i].color.W < 0.0f)shotFlg[i]= false;
				
				//if(shotSp[i].angle > 361.0f)shotFlg[i]= false;
				//shotSp[i].angle	+= 1.0f ;
				
				// 透明にしていき、完全に消えたらフラグをoffに
				if(shotSp[i].color.W > 0)	shotSp[i].color.W -= 0.01f;
				else shotFlg[i] = false;
				
				shotSp[i].size.Y -= 1.0f - shotSp[i].color.W;
				if(shotSp[i].size.Y <= 0.0f) shotSp[i].size.Y = 0.0f;
				
					
				shotAngleVec.X 	= (float)Math.Cos(FMath.Radians( shotSp[i].angle));
				shotAngleVec.Y 	= (float)Math.Sin(FMath.Radians( shotSp[i].angle));
				
				
				
				shotSp[i].pos	= new Vector3(AppMain.ScreenCenter.X + shotAngleVec.X * SHOT_TEX_CENTER.X
				                            , AppMain.ScreenCenter.Y + shotAngleVec.Y * SHOT_TEX_CENTER.X
				                            , 0.0f);
				
				
				
			}
			
			//ロングショット更新
			if(longShotFlg == true)
			{
				if(playerTouchFlg == false)
				{
					if(longShotSp.color.W > 0)	longShotSp.color.W -= 0.01f;
					else longShotFlg = false;
					
					longShotSp.size.Y -= 1.0f - longShotSp.color.W;
					if(longShotSp.size.Y <= 0.0f) longShotSp.size.Y = 0.0f;
				}
				else
				{
					colorCnt+=0.5f;
					longShotSp.size.Y = 3.0f+(float)Math.Sin(colorCnt);
					longShotSp.angle = FMath.Degrees(saveAngle);
					
				}
				
				
				shotAngleVec.X 	= (float)Math.Cos(FMath.Radians( longShotSp.angle));
				shotAngleVec.Y 	= (float)Math.Sin(FMath.Radians( longShotSp.angle));
				
				
				
				longShotSp.pos	= new Vector3(AppMain.ScreenCenter.X + shotAngleVec.X * SHOT_TEX_CENTER.X
				                            , AppMain.ScreenCenter.Y + shotAngleVec.Y * SHOT_TEX_CENTER.X
				                            , 0.0f);
				
				CheckCollision(L_RANGE, L_DAMAGE);
				playerTouchFlg = false;
			}
		}
		
		//描画
		public void Draw()
		{
			for (int i=0 ; i<SHOTMAX ; i++)
			{
				if(shotFlg[i] == true)
				{
					shotSp[i].Draw();
				}
			}
			if (longShotFlg == true)longShotSp.Draw();
		}
		
		//解放
		public void Term()
		{
			shotTex.Dispose();
		}
		
		//ノーマルレーザー作成関数
		public void Shot(float Angle, TimingRank Rank, int Combo)
		{
			//変数を一時保持
			saveRank = Rank;
			saveCombo= Combo;
			saveAngle= Angle;
			
			for (int i=0 ; i<SHOTMAX ; i++)
			{
				if(shotFlg[i] == false)
				{
					shotSp[i] = new Sprite2D(shotTex);
					shotFlg[i]= true;
					
					shotAngleVec.X 	= (float)Math.Cos(Angle);
					shotAngleVec.Y 	= (float)Math.Sin(Angle);
					
					longShotSp.pos	= new Vector3(AppMain.ScreenCenter.X + shotAngleVec.X * SHOT_TEX_CENTER.X
				                           		, AppMain.ScreenCenter.Y + shotAngleVec.Y * SHOT_TEX_CENTER.X
					                            , 0.0f);
					shotSp[i].size	= new Vector2(1.5f, 3.0f);
					shotSp[i].center = new Vector2(0.0f, 0.5f);
					shotSp[i].color = new Vector4(1.0f,0.0f,0.0f,1.0f);
					shotSp[i].angle	= FMath.Degrees(Angle);
					
					//当たり判定
					CheckCollision(N_RANGE, N_DAMAGE);
						
					break;
				}
			}
		}
		
		//ロングノート用レーザー作成関数
		public void LongShot(float Angle, TimingRank Rank, int Combo)
		{
			//変数を一時保持
			saveRank = Rank;
			saveCombo= Combo;
			saveAngle= Angle;
			playerTouchFlg = true;
			longShotSp.color.W = 1.0f;
			if(longShotFlg == false)
			{
				longShotSp = new Sprite2D(shotTex);
				longShotFlg = true;
				
				shotAngleVec.X 	= (float)Math.Cos(Angle);
				shotAngleVec.Y 	= (float)Math.Sin(Angle);
					
				longShotSp.pos	= new Vector3(AppMain.ScreenCenter.X + shotAngleVec.X * SHOT_TEX_CENTER.X
					                        , AppMain.ScreenCenter.Y + shotAngleVec.Y * SHOT_TEX_CENTER.X
				                            , 0.0f);
				longShotSp.size	= new Vector2(1.5f, 3.0f);
				longShotSp.color = new Vector4(1.0f,1.0f,0.0f,1.0f);
				longShotSp.center = new Vector2(0.0f, 0.5f);
				longShotSp.angle	= FMath.Degrees(Angle);
				colorCnt		= 0;
			}
			
		}
		
		//敵との当たり判定およびダメージ計算関数
		private void CheckCollision (float Range, float Damage)
		{
			foreach(Enemy enemy in GameScene.enemyManager.enemies)
			{
				float enmAngle;		// 自分から見た敵位置への角度
				float enmAngleCalc;	// 角度の＋－の統一時の角度
				enmAngle = FMath.Atan2(enemy.GetPos().Y - AppMain.ScreenCenter.Y,
				                       enemy.GetPos().X - AppMain.ScreenCenter.X);
			//if(Math.Abs(FMath.Degrees(enmAngle)) + Range >= 180.0f)
			//{
				if(FMath.Degrees(enmAngle) >0.0f) enmAngleCalc = -(180.0f +(180.0f - FMath.Degrees(enmAngle)));
				else enmAngleCalc = 180.0f +(180.0f + FMath.Degrees(enmAngle));
				//Console.WriteLine (FMath.Degrees(enmAngle));
			//}
				
				// ショット角度と敵角度がある程度同じならダメージを加える
				float damageBonus = 1.0f +0.5f * (2 - (int)saveRank);	// ボーナスダメージ
				if(FMath.Degrees(Math.Abs (saveAngle - enmAngle)) < Range ||
				   FMath.Degrees(Math.Abs (saveAngle - FMath.Radians(enmAngleCalc))) < Range)
				{
					enemy.AddDamage(Damage * damageBonus);
				}
			}
		}
	}
}

