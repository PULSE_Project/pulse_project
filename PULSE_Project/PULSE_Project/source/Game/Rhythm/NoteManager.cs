//--------------------------------------------------------------
// クラス名： NoteManager.cs
// コメント： 譜面、及びパルスリングの制御クラス
// 製作者　： ShionKubota
// 制作日時： 2014/03/03
//--------------------------------------------------------------
using System;
using System.Diagnostics;
using System.Collections.Generic;
using Sce.PlayStation.Core;

namespace PULSE_Project
{
	public class NoteManager
	{
		public NotesData notesData{ get; private set; }		// 譜面データ
		public List<PulseRing> pulseRings{ get; private set; }		// ノートを表すリング
		
		public Stopwatch stopWatch;
		private Music music;
		
		// コンストラクタ
		public NoteManager (MusicId musicId)
		{
			Init (musicId);
		}
		
		// 初期化
		private void Init(MusicId musicId)
		{
			// パルスリング
			pulseRings = new List<PulseRing>();
			
			// 譜面データ
			switch(musicId)
			{
			case MusicId.Cyber06:
				notesData = new NData_Cyber06();
				break;
			case MusicId.SIXTH_SENSE:
				notesData = new NData_SIXTH_SENSE();
				break;
			case MusicId.PowerForTommorow:
				notesData = new NData_PowerForTommorow();
				break;
			case MusicId.Celluloid_Splash:
				notesData = new NData_Celluloid_Splash();
				break;
			}
			
			// 曲情報保存
			PlayData.GetInstance().notesData = notesData;
			
			// 楽曲データ読み込み&再生
			music = new Music(@"/Application/assets/music/" + notesData.fileName);
			music.Set(false, 1.0f, 0.0f);
			
			// ストップウォッチ
			stopWatch = new Stopwatch();
			stopWatch.Start();
		}
		
		// 更新
		public void Update()
		{
			// タイミングをチェックしてパルスリングを生成
			ManageGenRing();
			
			// パルスリングの更新
			for(int i = pulseRings.Count - 1; i >= 0; i--)
			{
				pulseRings[i].Update();
				
				// 消去
				if(pulseRings[i].removeThis)
				{
					pulseRings[i].Term();
					pulseRings.RemoveAt(i);
				}
			}
		}
		
		// ノート情報を元にリングを生成---------------------------------------------
		private void ManageGenRing()
		{
			//foreach(Note note in notesData.notes)
			for(int i = 0; i < notesData.notes.Count; i++)	
			{
				Note curNote = notesData.notes[i];
				
				// ロングノートの開始ノートがクリア済み
				/*
				if(curNote.type == Note.Type.END && notesData.notes[i - 1].isClear)
				{
					curNote.isClear = true;
				}
				*/
				
				// 使い終わったノートはスルー
				if(curNote.isClear) continue;
				
				// タイミングが合っていればリングを生成
				if(IsGenTiming(curNote.timing))
				{
					pulseRings.Add(new PulseRing(curNote, notesData.speed, stopWatch.ElapsedMilliseconds));
				}
			}
		}
		
		//---------------------------------------------------------------------------------------
		// 渡されたタイミング(ノートがピッタリ中央に来るタイミング)を元にそのリングの発生時間を計算し、
		// その発生時間と現在時間がほぼ同じならtrueを返す。
		private bool IsGenTiming(long hitTiming)
		{
			long nowTime = stopWatch.ElapsedMilliseconds;
			
			// ノート出現からジャストタイミングにいたるまでの時間を算出
			float movingFrame = (PulseRing.DEFAULT_SCALE - PlayersUnit.SCALE) / notesData.speed;// 移動に何フレームかかるか
			long movingTime = (long)(movingFrame / 0.06f);										// それをミリ秒に変換
			
			// 出現タイミングを算出し、ジャストタイミングなら生成
			long genTiming = hitTiming - movingTime;					// 出現タイミング
			long justZone = 25L;					// 許容するズレ
			return (Math.Abs(genTiming - nowTime) <= justZone) ? true : false;
		}
		
		// 描画
		public void Draw()
		{
			foreach(PulseRing ring in pulseRings)
			{
				ring.Draw();
			}
		}
		
		// 解放
		public void Term()
		{
			foreach(PulseRing ring in pulseRings)
			{
				ring.Term();
			}
			for(int i = pulseRings.Count - 1; i >= 0; i--)
			{
				pulseRings[i].Term();
				pulseRings.RemoveAt(i);
			}
			
			music.Term();
		}
		
		// 譜面のノート数取得
		public int NoteNum{ get{ return notesData.notes.Count; }}
	}
}

