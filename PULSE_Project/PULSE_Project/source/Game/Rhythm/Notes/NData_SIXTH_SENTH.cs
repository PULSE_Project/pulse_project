using System;

namespace PULSE_Project
{
	public class NData_SIXTH_SENSE : NotesData
	{
		public NData_SIXTH_SENSE ()
		{
			fileName = "SIXTH_SENSE.mp3";
			id = MusicId.SIXTH_SENSE;
			length = 115000L;
			speed = 0.01f;
			
			// 譜面データ
			AddNote (5800L);
			AddNote (7460L);
			AddNote (10200L);
			AddNote (11900L);
			AddNote (13490L);
			AddNote (15230L);
			AddNote (17000L);
			AddNote (17850L,18800L);
			AddNote (20400L);
			AddNote (22200L);
			AddNote (23900L,25710L);
			AddNote (27400L);
			AddNote (29100L);
			AddNote (30500L);
			AddNote (31750L);
			AddNote (32100L);
			AddNote (33050L);
			AddNote (34800L);
			AddNote (35450L);
			AddNote (35650L);
			AddNote (37700L,39300L);
			AddNote (40100L);
			AddNote (40900L);
			AddNote (41100L,42800L);
			AddNote (43700L);
			AddNote (44900L);
			AddNote (45070L);
			AddNote (46460L);
			AddNote (47300L);
			AddNote (47850L);
			AddNote (49400L);
			AddNote (50710L);
			AddNote (50970L);
			AddNote (51360L);
			AddNote (51800L,53200);
			AddNote (54650L);
			AddNote (55500L);
			AddNote (56000L);
			AddNote (56950L);
			AddNote (58200L);
			AddNote (59500L);
			AddNote (60705L);
			AddNote (62700L);
			AddNote (63700L);
			AddNote (67220L);
			AddNote (67500L);
			AddNote (70640L,72310L);
			AddNote (74000L);
			AddNote (75820L);
			AddNote (77500L,79800L);
			AddNote (82600L);
			AddNote (84200L,86950L);
			AddNote (87760L);
			AddNote (88470L);
			AddNote (89600L);
			AddNote (91200L);
			AddNote (93000L,94500L);
			AddNote (98100L,99900L);
		}
		
		private void Init()
		{
			
		}
	}
}

