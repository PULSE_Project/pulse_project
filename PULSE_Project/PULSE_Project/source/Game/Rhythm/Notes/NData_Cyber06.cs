using System;

namespace PULSE_Project
{
	public class NData_Cyber06 : NotesData
	{	
		public NData_Cyber06 ()
		{
			fileName = "Cyber06.mp3";
			id = MusicId.Cyber06;
			length = 90000L;
			speed = 0.01f;
			
			// 譜面データ
			AddNote (7030L);
			AddNote (8502L);
			AddNote (9835L);
			AddNote (9980L);
			AddNote (13040L);
			AddNote (14630L);
			AddNote (15350L);
			AddNote (17630L);
			AddNote (18580L);
			AddNote (20700L);
			AddNote (21490L);
			AddNote (22590L);
			AddNote (23440L);
			AddNote (24630L);
			AddNote (27150L);
			AddNote (29120L);
			AddNote (30570L,31170L);
			AddNote (32090L);
			AddNote (34270L);
			AddNote (37500L);
			AddNote (38180L);
			AddNote (42710L);
			AddNote (43700L);
			AddNote (43950L);
			AddNote (45300L);
			AddNote (46420L);
			AddNote (47900L);
			AddNote (48510L);
			AddNote (49460L);
			AddNote (51270L,52390L);
			AddNote (54580L);
			AddNote (56670L);
			AddNote (57340L);
			AddNote (58190L);
			AddNote (58860L);
			AddNote (59240L);
			AddNote (63007L);
			AddNote (64027L,65310L);
			AddNote (66170L);
			AddNote (71570,73760L);
			AddNote (74030L);
			AddNote (74330L);
			AddNote (76800L);
			AddNote (77100L);
			AddNote (82950L);
			AddNote (83500L);
			AddNote (84407L);
			AddNote (84790L);
			AddNote (85167L);
			AddNote (85547L);
		}
		
		private void Init()
		{
			
		}
	}
}

