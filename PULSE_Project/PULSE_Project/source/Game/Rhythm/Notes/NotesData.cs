//--------------------------------------------------------------
// クラス名： NotesData.cs
// コメント： 譜面データ親クラス
// 製作者　： ShionKubota
// 制作日時： 2014/02/28
//--------------------------------------------------------------
using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;

namespace PULSE_Project
{
	public class NotesData
	{
		// 楽曲の情報
		public string fileName{ get; protected set; } // 楽曲ファイル名 "○○○.mp3"
		public MusicId id{ get; protected set; }	  // 楽曲ID
		public long length{ get; protected set; }	  // 曲の長さ
		
		// 譜面データ
		public List<Note> notes{ get; protected set; }
		public float speed{ get; protected set; }
		
		public NotesData()
		{
			notes = new List<Note>();
		}
		
		// ノートセット
		protected void AddNote(long timing)
		{
			notes.Add(new Note(timing, Note.Type.NORMAL));
		}
		// ロングノートセット
		protected void AddNote(long timing, long endTiming)
		{
			notes.Add(new Note(timing, Note.Type.START));
			notes.Add(new Note(endTiming, Note.Type.END));
		}
	}
}

