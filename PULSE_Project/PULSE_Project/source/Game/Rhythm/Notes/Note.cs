//--------------------------------------------------------------
// クラス名： Note.cs
// コメント： ノート(譜)クラス
// 製作者　： ShionKubota
// 制作日時： 2014/02/28
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core;

namespace PULSE_Project
{
	public class Note
	{
		public long timing{ get; private set; }
		public bool isClear;
		
		public enum Type
		{
			NORMAL,
			START,
			END
		}
		public Type type{ get; private set; }
		
		// 通常ノート
		public Note(long timing, Type type)
		{
			this.timing = timing;
			this.type = type;
			isClear = false;
		}
	}
}

