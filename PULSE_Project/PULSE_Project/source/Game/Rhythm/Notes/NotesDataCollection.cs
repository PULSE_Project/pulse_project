using System;

namespace PULSE_Project
{
	public sealed class NotesDataCollection
	{
		private static NotesDataCollection instance;
		public const int MUSIC_NUM = 3;
		private NotesData[] notesDatas;
		
		private NotesDataCollection ()
		{
			notesDatas = new NotesData[]{new NData_Cyber06(),
										 new NData_SIXTH_SENSE(),
										 new NData_PowerForTommorow(),
										 new NData_Celluloid_Splash()};
		}
		
		public NotesData this[int index]
		{
			get
			{
				return notesDatas[index];
			}
		}
		
		public static NotesDataCollection GetInstance()
		{
			if(instance == null) instance = new NotesDataCollection();
			return instance;
		}
	}
}

