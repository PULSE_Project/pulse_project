using System;

namespace PULSE_Project
{
	public class NData_Celluloid_Splash : NotesData
	{
		public NData_Celluloid_Splash ()
		{
			fileName = "Celluloid_Splash.mp3";
			id = MusicId.Celluloid_Splash;
			length = 97500L;
			speed = 0.01f;
			
			// 譜面データ
			AddNote (7100L);
			AddNote (9050L);
			AddNote (11100L,18100L);
			AddNote (19150L);
			AddNote (21150L);
			AddNote (23150L);
			AddNote (25150L);
			AddNote (27150L);
			AddNote (29150L);
			AddNote (31150L);
			AddNote (33150L);
			AddNote (35150L);
			
			AddNote (37100L);
			AddNote (37300L);
			AddNote (39100L);
			AddNote (39300L);
			AddNote (41100L,42000L);
			
			AddNote (45100L);
			AddNote (45300L);
			AddNote (47100L);
			AddNote (47300L);
			AddNote (49100L,50200L);
			
			AddNote (51100L);
			AddNote (51500L);
			AddNote (52100L);
			AddNote (53100L,54570L);
			
			AddNote (55100L);
			AddNote (55500L);
			AddNote (56050L);
			AddNote (56650L,58100L);
			
			AddNote (59200L);
			AddNote (59600L);
			AddNote (60200L);
			AddNote (61200L,62600L);
			
			AddNote (63100L);
			AddNote (65100L);
			AddNote (65500L);
			AddNote (66100L);
			
			AddNote (67100L);
			AddNote (68100L);
			AddNote (69100L);
			AddNote (70100L);
			AddNote (71100L,71700L);
			AddNote (72100L);
			
			AddNote (75100L);
			AddNote (76100L);
			AddNote (77100L);
			AddNote (77600L);
			AddNote (79100L,79700L);
			AddNote (80100L);
			
			AddNote (83200L,84700L);
		}
	}
}

