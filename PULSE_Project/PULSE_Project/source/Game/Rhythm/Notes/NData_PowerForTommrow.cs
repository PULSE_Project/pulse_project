using System;

namespace PULSE_Project
{
	public class NData_PowerForTommorow : NotesData
	{
		public NData_PowerForTommorow ()
		{
			fileName = "PowerForTommorow.mp3";
			id = MusicId.PowerForTommorow;
			length = 92000L;
			speed = 0.01f;
			
			// 譜面データ
			AddNote (7380L);
			AddNote (7900L,9050L);
			AddNote (9700L,11550L);
			AddNote (12450L,13900L);
			AddNote (14300L);
			AddNote (14550L);
			AddNote (15120L,17000L);
			AddNote (17580L,19300L);
			AddNote (19950L,22200L);
			AddNote (24130L);
			AddNote (24700L);
			AddNote (25700L);
			AddNote (26500L);
			AddNote (27635L);
			AddNote (28340L);
			AddNote (29370L);
			AddNote (30700L);
			AddNote (32450L);
			AddNote (34200L);
			AddNote (34680L);
			AddNote (35320L);
			AddNote (35880L);
			AddNote (36850L,39000L);
			AddNote (39500L);
			AddNote (40030L);
			AddNote (40810L);
			AddNote (41830L,43640L);
			AddNote (44250L);
			AddNote (44850L);
			AddNote (45670L);
			AddNote (46510L,48520L);
			AddNote (49020L);
			AddNote (49630L);
			AddNote (51070L);
			AddNote (53530L,54220L);
			AddNote (55630L);
			AddNote (56000L,57600L);
			AddNote (58300L);
			AddNote (59490L);
			AddNote (60670L,62060L);
			AddNote (63070L);
			AddNote (64340L);
			AddNote (65400L);
			AddNote (66590L);
			AddNote (67990L);
			AddNote (68970L);
			AddNote (70190L);
			AddNote (71450L);
			AddNote (71930L);
			AddNote (72200L);
			AddNote (72730L);
			AddNote (73900L);
			AddNote (74940L);
			AddNote (76150L);
			AddNote (77500L,78670L);
			AddNote (79550L);
			AddNote (79910L);
			AddNote (80870L);
			AddNote (82370L,84500L);
		}
		
		private void Init()
		{
			
		}
	}
}

