//--------------------------------------------------------------
// クラス名： PulseRing.cs
// コメント： ノート(譜)を表すリングのクラス
// 製作者　： ShionKubota
// 制作日時： 2014/03/03
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

namespace PULSE_Project
{
	public class PulseRing
	{
		public const float DEFAULT_SCALE = 3.0f;
		
		public Note note{ get; private set; }	// このリングが担当するノート情報
		public bool removeThis{ get; private set; }
		private long startTime;
		private float speed;
		private Sprite2D sp;
		private static Texture2D tex;
		
		public PulseRing (Note note, float speed, long startTime)
		{
			Init (note, speed, startTime);
		}
		
		private void Init(Note note, float speed, long startTime)
		{
			if(tex == null)
			{
				tex = new Texture2D(@"/Application/assets/img/PulseRing.png", false);
			}
			sp = new Sprite2D(tex);
			sp.pos = AppMain.ScreenCenter;
			sp.size = new Vector2(DEFAULT_SCALE, DEFAULT_SCALE);
			// 通常ノートは水色、ロングノートは黄色
			sp.color = note.type == Note.Type.START || note.type == Note.Type.END ? 
							new Vector4(1.0f, 1.0f, 0.0f, 1.0f) : new Vector4(0.0f, 1.0f, 1.0f, 1.0f);
			
			removeThis = false;
			this.speed = speed;
			this.note = note;
			this.startTime = startTime;
		}
		
		public void Update()
		{
			// 押された後の処理
			if(note.isClear)
			{
				removeThis = true;
				return;
			}
			
			// 縮小
			if(sp.size.X > 0)
			{
				//sp.size -= new Vector2(speed, speed);
				float timePer = (float)(GameScene.noteManager.stopWatch.ElapsedMilliseconds - startTime) /
					((float)note.timing - (float)startTime);
				float newSize = DEFAULT_SCALE - timePer * (DEFAULT_SCALE - PlayersUnit.SCALE);
				sp.size = new Vector2(newSize, newSize);
			}
			// 縮小しきったら消滅
			else
			{
				removeThis = true;
				note.isClear = true;
				
				PlayData.GetInstance().AddNoteRank(TimingRank.MISS);	// ミス回数をカウント
				GameScene.gameUI.DispTimingRank(TimingRank.MISS);		// タイミング評価演出「MISS」をUIに表示させる
				GameScene.playersUnit.ResetCombo();						// コンボをリセット
				
				if(note.type == Note.Type.START)
					GameScene.playersUnit.BreakLongNote();	// ロングノート状態を中断
				if(note.type == Note.Type.END)
					GameScene.playersUnit.QuitLongNote();	// ロングノート状態を終了
			}
		}
		
		public void Draw()
		{
			sp.Draw();
		}
		
		public void Term()
		{
			sp.Term();
		}
	}
}

