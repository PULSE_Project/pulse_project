using System;

namespace PULSE_Project
{
	public enum TimingRank
	{
		PERFECT,
		GOOD,
		BAD,
		MISS,
		NONE
	}
}

