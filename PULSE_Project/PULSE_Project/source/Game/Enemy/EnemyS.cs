//--------------------------------------------------------------
// クラス名： EnemyS.cs
// コメント： 小さい敵クラス
// 製作者　： MasayukiTada
// 制作日時： 2014/03/03
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core ;
using Sce.PlayStation.Core.Graphics ;

namespace PULSE_Project
{
	// 小さい敵クラス
	public class EnemyS : Enemy
	{
		private static Texture2D ENEMY_S_TEX = new Texture2D(@"/Application/assets/img/bugfly.png", false);
		private const float LIFE_MAX = 10.0f;		// ライフの初期値
		private const float SPEED_HOSEI = 0.0003f;	// スピードの補正値
		
		// コンストラクタ
		public EnemyS (Vector2 pos, float speed) : base(ENEMY_S_TEX, LIFE_MAX, 0.07f, 0.1f)
		{
			sp.pos = new Vector3(pos.X, pos.Y, 0.0f);
			sp.color = new Vector4(1.0f, 190.0f / 255.0f, 30.0f / 255.0f, 1.0f);
			this.speedMin = speed * SPEED_HOSEI;
		}
	}
}

