//--------------------------------------------------------------
// クラス名： Enemy.cs
// コメント： 敵基本クラス
// 製作者　： MasayukiTada
// 制作日時： 2014/03/03
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core ;
using Sce.PlayStation.Core.Graphics ;

namespace PULSE_Project
{
	// 敵親クラス
	public class Enemy
	{
		protected static Texture2D lifeTex = new Texture2D(@"/Application/assets/img/LifeGauge.png", false);			// ライフゲージ用テクスチャ
		protected static Texture2D damageTex = new Texture2D(@"/Application/assets/img/damageEffect.png", false);		// ダメージエフェクト用テクスチャ
		protected Animation sp;						// 自分用スプライト
		protected Sprite2D lifeGauge;				// ライフゲージ用スプライト
		protected Animation damageEff;				// ダメージ時エフェクト
		public float life{get; protected set;}		// ライフ		
		protected float speed;						// 移動スピード
		protected float speedMin;					// 最終的な移動スピード
		protected Vector2 moveVec;					// 移動方向のベクトル
		protected Vector3 length;					// 移動地点までの距離
		protected bool moveVecCheckFlg;				// 移動方向をチェックしたかどうか
		public bool removeThis{get; protected set;}	// 死んだかどうかのフラグ
		private float lifeMax, lifeEff;				// ライフの初期値保存用とエフェクト用変数
		private float scaleRaito;					// スプライトのサイズの割合
		protected float lifeGaugeWidth;				// ライフゲージの位置がずれるのを防ぐ変数
		
		// コンストラクタ
		public Enemy (Texture2D tex, float life, float sizeX, float sizeY)
		{			
			// 自分のスプライト初期化
			Vector2 aniSize = new Vector2(0.1f, 0.142f);
			sp = new Animation(tex, aniSize, 3, 0, 65, true, true, true);
			sp.pos = Vector3.Zero;
			sp.size = new Vector2(sizeX, sizeY);	
			
			// ライフゲージのスプライト初期化
			lifeGauge = new Sprite2D(lifeTex);
			lifeGauge.center = new Vector2(0.0f, 0.5f);	// ライフゲージの描画の中心を左端に
			lifeGauge.size = new Vector2(sizeX, sizeX);	
			lifeGauge.pos.Y = -100;						// 画面端にちらつくのを防ぐため天空の彼方へ
			lifeGauge.color = new Vector4(155.0f / 255.0f, 255.0f / 255.0f, 40.0f / 255.0f, 0.0f);
			lifeGaugeWidth = lifeGauge.width;	// ライフゲージがずれるのを防ぐ
			
			// ダメージエフェクト初期化
			Vector2 effSize = new Vector2(0.2f, 0.25f);
			damageEff = new Animation(damageTex, effSize, 2, 0, 17, false, true, false);
			damageEff.size = new Vector2(0.1f, 0.125f);
			damageEff.color = new Vector4(1.0f, 0.0f, 0.0f, 1.0f);
			
			// 各変数初期化
			scaleRaito 	= sizeX;
			this.life 	= this.lifeMax = this.lifeEff = life;
			moveVec 	= Vector2.Zero;
			removeThis 	= false;
			speed	 	= 0.004f;
			speedMin 	= 0.0f;		// 子クラスで変更してね☆
			moveVecCheckFlg = false;
			length 		= Vector3.Zero;
		}
		
		// 更新
		public void Update()
		{
			// ライフが0になって、エフェクトが終わったら死亡
			if(life <= 0)
			{
				sp.color.W = 0.0f;	// 透明にする
				if(damageEff.FrameNo == 17) removeThis = true;
			}
			
			sp.Updata();	// アニメーション
			Move ();		// 移動
			
			// だんだん遅く
			if(speed > speedMin) speed -= 0.00003f;
			else speed = speedMin;
			
			// 位置変更反映
			sp.pos.X += moveVec.X;
			sp.pos.Y += moveVec.Y;
			
			// ライフゲージは自分に追従
			lifeGauge.pos = new Vector3(sp.pos.X - (lifeGaugeWidth / 2.0f), sp.pos.Y + (sp.height / 2.0f), sp.pos.Z);	// 自身の真下に表示
			
			// ライフゲージをライフに同期
			SynchronizeLifGauge();
			
			// ライフゲージの色を変更
			if(lifeEff < lifeMax * 0.3f) lifeGauge.color = new Vector4(255.0f / 255.0f, 60.0f / 255.0f, 40.0f / 255.0f, lifeGauge.color.W);
			
			// ライフゲージを薄くしていく
			if(lifeGauge.color.W > 0.0f) lifeGauge.color.W -= 0.13f;
			else lifeGauge.color.W = 0.0f;	// 透明で固定
			
			// アニメーションのポジションはenemyに同期
			damageEff.pos = sp.pos;
			damageEff.Updata();	// ダメージ時アニメーション
		}
		
		// 描画
		public void Draw()
		{
			sp.AnimationDraw();	// アニメーション描画
			if(lifeGauge.color.W > 0.0f) lifeGauge.Draw();
			damageEff.AnimationDraw();
		}
		
		// 解放
		public void Term()
		{
			sp.Term();
			lifeGauge.Term();
			damageEff.Term();
		}			
		
		// 位置取得
		public Vector3 GetPos()
		{
			return sp.pos;
		}
		
		// サイズ取得
		public float GetWidth()
		{
			return sp.width;
		}
		
		// サイズ取得
		public float GetHeight()
		{
			return sp.height;
		}
		
		// ダメージをうける
		public void AddDamage(float damage)
		{
			if(life <= 0) return;
			if(damage < 0) life = 0;		// 強制的に消滅
			else 		   life -= damage;	// ダメージ分ライフから減らす
			damageEff.Set(0, 17);			// エフェクト再生
		}
		
		// ライフゲージをライフに同期させる処理
		protected void SynchronizeLifGauge()
		{
			if(life < lifeEff)
			{
				lifeGauge.color.W = 1.0f;
				lifeEff -= 0.23f;
			}
			else lifeEff = life;
			
			lifeGauge.size.X = ((lifeEff / lifeMax)) * scaleRaito;
		}
		
		// 移動処理
		virtual protected void Move()
		{
			// 一回だけ移動方向を検出
			if(moveVecCheckFlg == false)
			{
				length = AppMain.ScreenCenter - sp.pos;	// 中心への距離を出す
				sp.angle = FMath.Degrees(FMath.Atan2(length.Y, length.X)) + 90.0f;	// キャラの角度を進行方向へ
				length.Normalize();						// 距離を正規化
				moveVecCheckFlg  = true;
			}
			
			// 移動値を反映
			moveVec.X = length.X * speed;
			moveVec.Y = length.Y * speed;
		}
		
		// 移動方向反転
		public void ReversalVec()
		{
			speed = Math.Abs(speed) * -1;
		}
	}
}

