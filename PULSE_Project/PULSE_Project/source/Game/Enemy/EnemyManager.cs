//--------------------------------------------------------------
// クラス名： EnemyManager.cs
// コメント： 敵管理クラス
// 製作者　： MasayukiTada
// 制作日時： 2014/03/03
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core ;
using Sce.PlayStation.Core.Graphics ;
using System.Collections.Generic;

namespace PULSE_Project
{
	// 敵管理クラス
	public class EnemyManager
	{
		enum ENEMY_TYPE
		{
			SMALL,
			LARGE,
			MAX
		};
		
		public List<Enemy> enemies{ get; private set; }		// 敵格納配列
		int count;					// 敵出現管理用カウンター
		int setOffset;				// 敵の出現間隔(ノートの数に応じて変更される)
		Random random;				// ランダム関数用
		
		public const float ENEMY_CIRCLE_R = 600.0f;	// 敵を出現させる軌道円の半径
		const int ONES_ADD_ENEMY_MAX = 3;			// 一度に出す敵の数
		const int ENEMY_LIMIT = 27;					// 画面内に出現させる敵の最大数
		
		// コンストラクタ
		public EnemyManager ()
		{
			Init ();
		}
		
		// 初期化
		int Init()
		{
			enemies = new List<Enemy>();	// 敵艦利用リストを生成
			count = 0;
			setOffset = (int)(GameScene.noteManager.notesData.length / 10) / GameScene.noteManager.notesData.notes.Count;	// 敵の出現間隔をノートの数に応じて変更
			random = new Random();
			
			// とりあえず出しちゃう
			for(int i = 0;i < ONES_ADD_ENEMY_MAX*2;i++)	AddEnemy(random.Next(0, 3), (float)random.Next(1, 7), (float)(random.Next(0, 45) * 8));

			return 0;
		}
		
		// 更新
		public void Update()
		{
			count++;	// フレーム数カウント
			
			// 敵更新&死亡チェック
			for(int i = enemies.Count - 1; i >= 0; i--)
			{
				enemies[i].Update();
				if(enemies[i].removeThis)
				{
					enemies[i].Term();
					enemies.RemoveAt(i);		// 死んでいる敵から消していく
					PlayData.GetInstance().defeatedEnemyNum++;	// 倒した敵の数をカウント
				}
			}
			
			if(count > setOffset)
			{
				// 敵の数が多すぎると生成しない
				if(enemies.Count < ENEMY_LIMIT)
					for(int i = 0;i < ONES_ADD_ENEMY_MAX;i++) AddEnemy(random.Next(0, 3), (float)random.Next(1, 7), (float)(random.Next(0, 45) * 8));
				count = 0;
			}
			
			CollisionWithPlayer();	// プレーヤーと各敵の当たり判定処理
			
			// 最後のノートが終わったら敵を反転
			if(GameScene.noteManager.notesData.notes[GameScene.noteManager.notesData.notes.Count - 1].isClear)
			{
				foreach(Enemy enemy in enemies)
				{
					enemy.ReversalVec();
				}				
			}
		}
		
		// 描画
		public void Draw()
		{
			foreach(Enemy enemy in enemies)
			{
				enemy.Draw ();
			}
		}	
		
		// 解放
		public void Term()
		{
			// すべての敵を解放する
			for(int i = enemies.Count - 1; i >= 0; i--)
			{
				enemies[i].Term();
				enemies.Remove(enemies[i]);
			}
		}
		
		// 敵出現処理
		void AddEnemy(int type, float speed, float angle)
		{			
			// 受け取った角度の軌道円上にセット
			Vector2 pos;
			pos.X = ENEMY_CIRCLE_R * FMath.Cos(FMath.Radians(angle));
			pos.Y = ENEMY_CIRCLE_R * FMath.Sin(FMath.Radians(angle));
			
			switch(type)
			{
			case 0:
			case 1:
				enemies.Add(new EnemyS(new Vector2(AppMain.ScreenCenter.X + pos.X, AppMain.ScreenCenter.Y + pos.Y), speed));	// 小さい敵
				break;
			case 2:
				enemies.Add(new EnemyL(new Vector2(AppMain.ScreenCenter.X + pos.X, AppMain.ScreenCenter.Y + pos.Y), speed));	// 大きい敵
				break;
			default :	// バグで出現しない状況を回避
				enemies.Add(new EnemyS(new Vector2(AppMain.ScreenCenter.X + pos.X, AppMain.ScreenCenter.Y + pos.Y), speed));	// 小さい敵
				break;
			}
		}
		
		// 敵と自機の当たり判定
		void CollisionWithPlayer()
		{
			foreach(Enemy enemy in enemies)
			{
				Vector3 posE = enemy.GetPos();			// 敵のポジション
				Vector3 posP = AppMain.ScreenCenter;	// プレーヤーのポジション
				float radiusP = PlayersUnit.RADIUS;		// プレーヤーの半径
				float x = posP.X - posE.X;				// プレーヤーから敵の距離X
				float y = posP.Y - posE.Y;				// プレーヤーから敵の距離Y
				float len = ((x * x) + (y * y));		// プレーヤーから敵の距離L
				
				if((radiusP * radiusP) > len)
				{
					if(enemy.life > 0)
					{
						GameScene.playersUnit.AddDamage(0.5f);	// プレイヤーにダメージ
						enemy.AddDamage(-1.0f);	// 消滅
						PlayData.GetInstance().defeatedEnemyNum--;	// 倒した敵の数から1引く						
					}
				}
			}
		}
	}
}

