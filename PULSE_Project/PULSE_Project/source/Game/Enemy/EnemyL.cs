//--------------------------------------------------------------
// クラス名： EnemyL.cs
// コメント： 大きい敵クラス
// 製作者　： MasayukiTada
// 制作日時： 2014/03/03
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core ;
using Sce.PlayStation.Core.Graphics ;

namespace PULSE_Project
{
	// 大きい敵スラス
	public class EnemyL : Enemy
	{
		private static Texture2D ENEMY_L_TEX = new Texture2D(@"/Application/assets/img/bugfly.png", false);
		private const float LIFE_MAX = 20.0f;		// ライフの初期値
		private const float SPEED_HOSEI = 0.00008f;	// スピードの補正値
		
		// コンストラクタ
		public EnemyL (Vector2 pos, float speed) : base(ENEMY_L_TEX, LIFE_MAX, 0.14f, 0.2f)
		{
			sp.pos = new Vector3(pos.X, pos.Y, 0.0f);
			sp.color = new Vector4(1.0f, 120.0f / 255.0f, 170.0f / 255.0f, 1.0f);
			this.speedMin = speed * SPEED_HOSEI;
		}
	}
}

