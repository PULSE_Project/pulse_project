//--------------------------------------------------------------
// クラス名： GameUI.cs
// コメント： ゲーム中のUIを管理するクラス
// 製作者　： ShionKubota
// 制作日時： 2014/03/05
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

namespace PULSE_Project
{
	public class GameUI
	{
		// タイミング評価の表示エフェクト
		private const float TIMING_RANK_NUM = 3;
		private Sprite2D timingRankSp;
		private Texture2D timingRankTex;
		private float timingRankRotation;
		
		// コンストラクタ
		public GameUI ()
		{
			Init ();
		}
		
		// 初期化
		private void Init()
		{
			// タイミング評価関連
			timingRankTex = new Texture2D(@"/Application/assets/img/TimingRank.png", false);
			timingRankSp = new Sprite2D(timingRankTex);
			timingRankSp.pos = AppMain.ScreenCenter;
			timingRankSp.textureUV = new Vector4(0.0f, 0.0f, 0.5f, 0.5f);
			timingRankSp.size = new Vector2(0.2f, 0.2f);
			timingRankSp.color.W = 0.0f;
			timingRankRotation = 0.0f;
		}
		
		// 更新
		public void Update()
		{
			ManageTimingRank();	// タイミング評価演出 関連処理
		}
		
		// タイミング評価演出の関連処理------------------------------------
		private void ManageTimingRank()
		{
			const float fadeOutSpeed = 0.01f;	// フェードアウトの速度
			const float rotationDecel = 2.0f;	// 回転の減速量
			
			// 透明になりながら回転
			if(timingRankSp.color.W > 0.0f)
			{
				timingRankSp.color.W -= fadeOutSpeed;
				timingRankSp.angle += timingRankRotation;
			}
			
			// 回転量を減速
			if(timingRankRotation > 0.0f)
			{
				timingRankRotation -= rotationDecel;
			}
		}
		
		// 描画
		public void Draw()
		{
			timingRankSp.Draw();
		}
		
		// 解放
		public void Term()
		{
			timingRankTex.Dispose();
		}
		
		// タイミングの評価を表示
		public void DispTimingRank(TimingRank rank)
		{
			timingRankSp.color.W = 1.0f;
			timingRankRotation = 30.0f;
			
			switch(rank)
			{
			case TimingRank.PERFECT:
				timingRankSp.textureUV = new Vector4(0.0f, 0.0f, 0.5f, 0.5f);
				break;
			case TimingRank.GOOD:
				timingRankSp.textureUV = new Vector4(0.5f, 0.0f, 1.0f, 0.5f);
				break;
			case TimingRank.BAD:
				timingRankSp.textureUV = new Vector4(0.0f, 0.5f, 0.5f, 1.0f);
				break;
			case TimingRank.MISS:
				timingRankSp.textureUV = new Vector4(0.5f, 0.5f, 1.0f, 1.0f);
				break;
			default:
				break;
			}
		}
	}
}

