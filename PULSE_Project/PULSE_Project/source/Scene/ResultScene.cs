//--------------------------------------------------------------
// クラス名： Result.cs
// コメント： リザルト画面クラス
// 製作者　： MasayukiTada
// 制作日時： 2014/02/28
// 更新日時： 2014/02/28
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.Core.Input;
using Sce.PlayStation.HighLevel.UI;

namespace PULSE_Project
{
	public class ResultScene : BaseScene
	{	
		//リザルトシーン内の処理手順
		enum RESULT_ENUM
		{
			SLIDE,		//タイミング判定のイン
			TIMINGNUM,	//タイミング判定のカウントアップ
			SCORE,		//スコアのカウントアップ
			ENEMY,		//スコアに敵撃破のスコアを加算
			RANK,		//ランクの表示
			END			//終了
		}
		
		RESULT_ENUM oder;
		private int frameCnt;
		//背景
		private Sprite2D backSp;
		private Texture2D backTex;
		
		//プレイヤーキャラ
		private Sprite2D myCharSp;
		private Texture2D myCharTex;
		
		//1文字ランク
		private Animation rankSp;
		private Texture2D rankTex;
		private Vector4 RANK_CUT = new Vector4(128.0f / 512.0f, 128.0f / 128.0f, 128.0f, 128.0f);
		
		//各ノート判定[判定4種]
		private const int TIMING_RANK_KIND = 4;
		private Sprite2D[] timingRankSp =new Sprite2D[TIMING_RANK_KIND];
		private Texture2D timingRankTex;
		private Vector2[] timingPosArray = new Vector2[TIMING_RANK_KIND];
		private float[] nowTimingPosAngle = new float[TIMING_RANK_KIND];
		private int[] timingAngleWay = new int[TIMING_RANK_KIND];
		
		//文字表示系
		private Vector2[] RESULT_CHAR_SIZE = new Vector2[2];
		private Texture2D resultCharTex;
		private Vector4 RESULT_CUT = new Vector4(256.0f / 1024.0f, 64.0f / 512.0f, 256.0f, 64.0f);
		//「music」
		private Sprite2D musicCharSp;
		//「曲名」
		private Sprite2D titleCharSp;
		//「score」
		private Sprite2D scoreCharSp;
		//「enemy」
		private Sprite2D enemyCharSp;
		
		//数値表示系
		private const int DIGIT = 2;
		private const int SCORE_DIGIT = 8;
		private const int ENEMY_DIGIT = 4;
		private Vector4 NUMBER_CUT = new Vector4(128.0f / 512.0f, 128.0f / 512.0f , 128.0f, 128.0f);
		private const float NUMBER_SCALE = 0.7f;
		private Texture2D numberTex;
		
		//ノート判定の数[判定4種][2桁]
		private Vector2 NOTE_RANK_CUT	= new Vector2(512.0f / 1024.0f, 512.0f / 1024.0f);
		private Animation [,] noteRankNumSp = new Animation[TIMING_RANK_KIND,DIGIT];
		private int[] maxNoteRankNum	= new int [TIMING_RANK_KIND];
		private int[] nowNoteRankNum	= new int [TIMING_RANK_KIND];
		//コンボ数[桁数]
		private Sprite2D[] comboSp = new Sprite2D[DIGIT];
		private int maxConbo;
		private int nowConbo;
		
		//スコア[SCORE_DIGIT]
		private Animation[] scoreSp = new Animation[SCORE_DIGIT];
		private int maxScore;
		private int nowScore;
		
		//敵撃破数[ENEMY_DIGIT]
		private Animation[] enemySp = new Animation[ENEMY_DIGIT];
		private int maxEnemy;
		private int nowEnemy;
		private int enemyPoint;				//敵１体辺りの得点
		// コンストラクタ
		public ResultScene ()
		{
			Init ();
		}
		
		// 初期化
		override public void Init()
		{
			PlayData data = PlayData.GetInstance();
			
			//Console.WriteLine ("Combo = " + data.comboMax);
			//Console.WriteLine ("Perfect = " + data.notePerfectNum);
			//Console.WriteLine ("Miss    = " + data.noteMissNum);
			
			//リザルトシーンの処理
			oder = RESULT_ENUM.SLIDE;
			frameCnt = 0;
			
			//テクスチャとスプライトの初期化
			
			//背景
			//backTex = new Texture2D (@"/Application/assets/img/back.png", false);
			//backSp = new Sprite2D(backTex);
			
			//自機
			myCharTex 		= new Texture2D (@"/Application/assets/img/CentralUnit.png", false);
			myCharSp 		= new Sprite2D(myCharTex);
			myCharSp.pos	= new Vector3(AppMain.ScreenWidth/2.0f,AppMain.ScreenHeight,0);
			myCharSp.size	= new Vector2 (0.75f,0.75f);
			myCharSp.color	= new Vector4(1.0f,1.0f,1.0f,1.0f);
			myCharSp.angle	= 0.0f;
				
			//1文字ランク
			rankTex 	=	new Texture2D (@"/Application/assets/img/Rank.png", false);
			rankSp 		= new Animation(rankTex, new Vector2(RANK_CUT.X, RANK_CUT.Y), 1, 0, 4, false, false, true);
			rankSp.pos	= new Vector3(AppMain.ScreenWidth/2.0f, AppMain.ScreenHeight/2.0f, 0.0f);
			rankSp.size	= new Vector2(RANK_CUT.X, RANK_CUT.Y);
			rankSp.FrameNo = 0;
			rankSp.color.W = 0.0f;
			
			//各ノートの判定
			timingRankTex = new Texture2D (@"/Application/assets/img/TimingRank.png", false);
			//各ノートのポジションの設定(Vec2(距離,角度))
			timingPosArray[0] = new Vector2(300.0f, 20.0f - 180.0f);
			timingPosArray[1] = new Vector2(300.0f, 55.0f - 180.0f);
			timingPosArray[2] = new Vector2(300.0f, 125.0f - 180.0f);
			timingPosArray[3] = new Vector2(300.0f, 160.0f - 180.0f);
			

			
			for(int i=0 ; i<TIMING_RANK_KIND ; i++)
			{
				timingRankSp[i] 		= new Sprite2D(timingRankTex);
				timingRankSp[i].pos		= new Vector3(myCharSp.pos.X + timingPosArray[i].X * FMath.Cos(FMath.Radians(timingPosArray[i].Y + 180.0f))
				                                     ,myCharSp.pos.Y + timingPosArray[i].X * FMath.Sin(FMath.Radians(timingPosArray[i].Y + 180.0f))
				                                   ,0.0f);
				timingRankSp[i].size	= new Vector2(NOTE_RANK_CUT.X * 0.4f, NOTE_RANK_CUT.Y * 0.4f);
				timingRankSp[i].textureUV=new Vector4(NOTE_RANK_CUT.X * (i%2), NOTE_RANK_CUT.Y * (i/2),NOTE_RANK_CUT.X * (i%2+1), NOTE_RANK_CUT.Y * (i/2+1));
				timingRankSp[i].angle	= 0.0f;
				
				//移動中のスライドの角度
				nowTimingPosAngle[i] 	= 0.0f;
				timingAngleWay[i]		= i%2 == 0 ? 1:-1;
			}
			
			//各文字
			RESULT_CHAR_SIZE[0] = new Vector2(256.0f , 64.0f);
			RESULT_CHAR_SIZE[1] = new Vector2(512.0f , 64.0f);
			
			resultCharTex = new Texture2D (@"/Application/assets/img/ResultChar.png", false);
			
			//「MUSIC」中心点左上から。POSは微調整
			musicCharSp 			= new Sprite2D(resultCharTex);
			musicCharSp.pos			= new Vector3(16.0f,16.0f,0.0f);
			musicCharSp.size		= new Vector2(RESULT_CUT.X, RESULT_CUT.Y);
			musicCharSp.center		= new Vector2(0.0f, 0.0f);
			musicCharSp.textureUV 	= new Vector4(0.0f, 0.0f, RESULT_CUT.X, RESULT_CUT.Y);
			
			//「SCORE」中心点中央
			scoreCharSp 			= new Sprite2D(resultCharTex);
			scoreCharSp.pos			= new Vector3(AppMain.ScreenWidth/4.0f, AppMain.ScreenHeight / 5.0f,0.0f);
			scoreCharSp.size		= new Vector2(RESULT_CUT.X, RESULT_CUT.Y);
			scoreCharSp.textureUV 	= new Vector4(RESULT_CUT.X*1.0f, 0.0f, RESULT_CUT.X*2.0f, RESULT_CUT.Y);
			
			//「TITLE」中心点左中央から。MUSIC＋微調整
			titleCharSp 			= new Sprite2D(resultCharTex);
			titleCharSp.pos			= new Vector3(musicCharSp.pos.X + 256.0f, musicCharSp.pos.Y, 0.0f);
			titleCharSp.size		= new Vector2(1.0f, 64.0f / 512.0f);
			titleCharSp.center		= new Vector2(0.0f, 0.0f);
			titleCharSp.textureUV 	= new Vector4(0.0f, (64.0f / 512.0f)*(int)(GameScene.musicId + 1), 1.0f, (64.0f / 512.0f)*(int)(GameScene.musicId + 2));
			
			//「ENEMY」中心点中央から。SCORE+Y微調整
			enemyCharSp 			= new Sprite2D(resultCharTex);
			enemyCharSp.pos			= new Vector3(scoreCharSp.pos.X, scoreCharSp.pos.Y + 64.0f, 0.0f);
			enemyCharSp.size		= new Vector2(RESULT_CUT.X, RESULT_CUT.Y);
			enemyCharSp.textureUV 	= new Vector4(RESULT_CUT.X*2.0f, 0.0f, RESULT_CUT.X*3.0f, RESULT_CUT.Y);
			
			
			//各数字
			numberTex = new Texture2D (@"/Application/assets/img/Number.png", false);
			
			//各ノートの判定の数字
			for(int i = 0 ; i<TIMING_RANK_KIND ; i++)
			{
				for (int j = 0 ; j<DIGIT ; j++)
				{
					noteRankNumSp[i,j] 		= new Animation(numberTex,new Vector2(NUMBER_CUT.X,NUMBER_CUT.Y),1,0,10,false,false,true);
					noteRankNumSp[i,j].pos	= new Vector3(timingRankSp[i].pos.X + (DIGIT/ 4.0f) * (NUMBER_CUT.Z/2.5f) - (NUMBER_CUT.Z/2.5f) *j
					                                     ,timingRankSp[i].pos.Y
					                                     , 0.0f);
					noteRankNumSp[i,j].size	= new Vector2(NUMBER_CUT.X * NUMBER_SCALE, NUMBER_CUT.Y*NUMBER_SCALE);
					noteRankNumSp[i,j].FrameNo = 0;
				}
				//初期化
				nowNoteRankNum[i] = 0;
			}
			//各最大値を各ノート判定の取得数に
			maxNoteRankNum[0] = data.notePerfectNum;
			maxNoteRankNum[1] = data.noteGoodNum;
			maxNoteRankNum[2] = data.noteBadNum;
			maxNoteRankNum[3] = data.noteMissNum;
			
			
			
			//コンボ
			for (int i = 0 ; i<DIGIT ; i++)
			{
				comboSp[i] = new Sprite2D(numberTex);
				comboSp[i].pos	= new Vector3(myCharSp.pos.X + (i*32.0f), myCharSp.pos.Y - 64.0f, 0.0f);
				comboSp[i].size	= new Vector2(NUMBER_CUT.X * NUMBER_SCALE,NUMBER_CUT.Y*NUMBER_SCALE);
				comboSp[i].textureUV 	= new Vector4(NUMBER_CUT.X * i, 0.0f, NUMBER_CUT.X * (i+1), NUMBER_CUT.Y);
			}
			maxConbo = data.comboMax;
			nowConbo = 0;
			//スコア
			for (int i = 0 ; i<SCORE_DIGIT ; i++)
			{
				scoreSp[i] 			= new Animation(numberTex,new Vector2(NUMBER_CUT.X,NUMBER_CUT.Y),1,0,10,false,false,true);
				scoreSp[i].pos		= new Vector3(scoreCharSp.pos.X +(SCORE_DIGIT/2.0f * NUMBER_CUT.Z / 1.0f) - i*(NUMBER_CUT.Z/2.5f), scoreCharSp.pos.Y, 0.0f);
				scoreSp[i].size		= new Vector2(NUMBER_CUT.X*NUMBER_SCALE,NUMBER_CUT.Y*NUMBER_SCALE);
				scoreSp[i].FrameNo 	= 0;
				//scoreSp[i].size	= new Vector2(NUMBER_CUT.X, 1.0f);
				
			}
			
			//スコアの計算　できるだけ誤差を無くすため大きな数値で割り最下位を0:切捨て　それ以外切り上げし、元の桁に戻す
			long scorePrice = (long)Math.Round((double)(100000000000000 / data.notesData.notes.Count)) ;
		
			//1個当たりの点数＊各ノートの数および倍率から計算
			long scoreCalc = 	(long)((scorePrice * data.notePerfectNum * 1.0f) 
						+ (scorePrice * data.noteGoodNum * 0.7f)
						+ (scorePrice * data.noteBadNum * 0.4f));
			maxScore = (int)(scoreCalc / 10000000);
			scorePrice = (int)(scorePrice / 10000000);
			
			//下2桁を0にする　中途半端な数になるので見栄えを良くする
			maxScore /=100;
			maxScore *=100;
			nowScore = 0;
			
			scorePrice /= 100;
			scorePrice *= 100;
			
			//敵の撃破数
			for (int i = 0 ; i<ENEMY_DIGIT ; i++)
			{
				enemySp[i] 			= new Animation(numberTex,new Vector2(NUMBER_CUT.X,NUMBER_CUT.Y),1,0,10,false,false,true);
				enemySp[i].pos		= new Vector3(scoreSp[i].pos.X, scoreSp[i].pos.Y + 64.0f, 0.0f);
				enemySp[i].size		= new Vector2(NUMBER_CUT.X*NUMBER_SCALE,NUMBER_CUT.Y*NUMBER_SCALE);
				enemySp[i].FrameNo 	= 0;
				
			}
			//撃破数の取得と初期化
			maxEnemy = data.defeatedEnemyNum;
			nowEnemy = 0;
			enemyPoint = (int)scorePrice;
			
			Console.WriteLine("Life" + data.remainingLife);
			Console.WriteLine("Point" + enemyPoint);
			Console.WriteLine("Enemy" + data.defeatedEnemyNum);
			//Console.WriteLine("perfect" + data.notePerfectNum);
		}
		
		
		// 更新
		override public void Update()
		{
			frameCnt++;
			
			
			for (int i=0 ; i<TIMING_RANK_KIND ; i++)
			{
				timingRankSp[i].angle += timingAngleWay[i]*1.0f;
			}
			
			//処理順用スイッチ
			switch(oder)
			{
			//ノート判定の4つが円形にスライドイン
			case RESULT_ENUM.SLIDE:
				frameCnt +=2;
				
				for (int i = 0; i<TIMING_RANK_KIND ; i++)
				{
					nowTimingPosAngle[i] += 3-(i*0.20f);
					if(180.0f + timingPosArray[i].Y - nowTimingPosAngle[i] >= timingPosArray[i].Y)
					timingRankSp[i].pos		= new Vector3(myCharSp.pos.X + timingPosArray[i].X * FMath.Cos(FMath.Radians(timingPosArray[i].Y + 180.0f - nowTimingPosAngle[i]))
				                                     	, myCharSp.pos.Y + timingPosArray[i].X * FMath.Sin(FMath.Radians(timingPosArray[i].Y + 180.0f - nowTimingPosAngle[i]))
				                                   		, 0.0f);
				}
				if(180.0f + timingPosArray[0].Y - nowTimingPosAngle[0] <= timingPosArray[0].Y &&
				   180.0f + timingPosArray[1].Y - nowTimingPosAngle[1] <= timingPosArray[1].Y &&
				   180.0f + timingPosArray[2].Y - nowTimingPosAngle[2] <= timingPosArray[2].Y &&
				   180.0f + timingPosArray[3].Y - nowTimingPosAngle[3] <= timingPosArray[3].Y ) 
				{
					frameCnt = 0;
					oder = RESULT_ENUM.TIMINGNUM;
					for (int i = 0 ; i<TIMING_RANK_KIND ; i++)
					{
						for (int j = 0; j <DIGIT ; j++)
						{
							noteRankNumSp[i,j].pos	= new Vector3(timingRankSp[i].pos.X + (DIGIT/ 4.0f) * (NUMBER_CUT.Z/2.5f) - (NUMBER_CUT.Z/2.5f) *j
					                                     ,timingRankSp[i].pos.Y
					                                     , 0.0f);
						}
					}
				}
				break;
			
			//ノート判定の数字カウントアップ
			case RESULT_ENUM.TIMINGNUM:
				
				if(frameCnt % 4 == 0)
				{
					for(int i=0 ; i<TIMING_RANK_KIND ; i++)
					{
						nowNoteRankNum[i]+=1;
						if(nowNoteRankNum[i] > maxNoteRankNum[i]) nowNoteRankNum[i] = maxNoteRankNum[i];
						
						
					}
					//数値を配列各桁に変換関数
					ArraySplit();
				}
				//終了条件：全ノート判定が指定値までカウントアップ、または○が押される
				if(nowNoteRankNum[0] >= maxNoteRankNum[0] &&
				   nowNoteRankNum[1] >= maxNoteRankNum[1] && 
				   nowNoteRankNum[2] >= maxNoteRankNum[2] && 
				   nowNoteRankNum[3] >= maxNoteRankNum[3] ||
				   Input.GetInstance().circle.isPushStart == true) 
				{
					//指定値にする
					for(int i=0 ; i<TIMING_RANK_KIND ; i++)
					{
						nowNoteRankNum[i] = maxNoteRankNum[i];
					}
					ArraySplit();
					
					//カウントリセットおよび次の処理へ
					frameCnt = 0;
					oder = RESULT_ENUM.SCORE;
				}	
				break;
				
			//スコアカウントアップ
			case RESULT_ENUM.SCORE:
				
				if(frameCnt % 2 == 0)
				{
					int calcNum = 0;
					if((maxScore - nowScore) * 0.1f < 1.0f) calcNum = 1;
					else calcNum = (int)((maxScore - nowScore) * 0.1f);
					
					nowScore += calcNum;
					//数値を配列各桁に変換関数
					ArraySplit();
				}
				//スコアが指定値までカウントアップまたは、○が押されたら
				if(nowScore > maxScore || Input.GetInstance().circle.isPushStart == true) 
				{
					nowScore = maxScore;
					ArraySplit();
					frameCnt = 0;
					oder = RESULT_ENUM.ENEMY;
				}	
				break;
			
			//敵撃破数をスコアに加算
			case RESULT_ENUM.ENEMY:
				if(frameCnt % 2 == 0)
				{
					if(nowEnemy < maxEnemy)
					{
						nowEnemy++;
						maxScore += enemyPoint;
					}
					
					int calcNum = 0;
					if((maxScore - nowScore) * 0.1f < 1.0f) calcNum = 1;
					else calcNum = (int)((maxScore - nowScore) * 0.1f);
					
					nowScore += calcNum;
					//数値を配列各桁に変換関数
					ArraySplit();
				}
				//スコアと撃破数が指定値までカウントアップまたは、○が押されたら
				if(nowScore > maxScore &&
				   nowEnemy == maxEnemy ||
				   Input.GetInstance().circle.isPushStart == true) 
				{
					maxScore += (maxEnemy - nowEnemy) * enemyPoint;
					nowScore = maxScore;
					nowEnemy = maxEnemy;
					ArraySplit();
					frameCnt = 0;
					oder = RESULT_ENUM.RANK;
					
					//スコアからランク確定
					int checkRank = maxScore / 100000;
					if(checkRank >= 175) rankSp.FrameNo = 0;
					else if(checkRank < 175 && checkRank >= 160) rankSp.FrameNo = 1;
					else if(checkRank < 160 && checkRank >= 150) rankSp.FrameNo = 2;
					else if(checkRank < 150) rankSp.FrameNo = 3;
				}	
				break;
				
			//ランクの表示
			case RESULT_ENUM.RANK:
				rankSp.color.W+=0.1f;
				if(rankSp.color.W >1.0f)
				{
					rankSp.color.W = 1.0f;
					oder = RESULT_ENUM.END;
				}
				break;
				
			//終了待ち
			case RESULT_ENUM.END:
				Input input = Input.GetInstance();
				if(input.circle.isPushEnd || input.cross.isPushEnd || input.triangle.isPushEnd ||
			   input.square.isPushEnd || input.triggerR.isPushEnd || input.triggerL.isPushEnd ||
			   input.start.isPushEnd || input.select.isPushEnd)
		 		{
					AppMain.sceneManager.Switch(SceneId.TITLE);
				}
				break;
			}
			
		}
		
		// 解放
		override public void Term()
		{
			myCharTex.Dispose();	
			timingRankTex.Dispose();
			rankTex.Dispose();
			resultCharTex.Dispose();
			numberTex.Dispose();
			
		}
		
		// 描画
		override public void Draw()
		{
			//背景
			//backSp.Draw();
			background.Draw();
			//プレイヤーキャラ
			myCharSp.Draw();
			
			//1文字ランク
			rankSp.Draw();
			
			//各ノート判定[判定4種]
			for(int i = 0 ; i<TIMING_RANK_KIND ; i++)
			{
				timingRankSp[i].Draw();
			}
			
			//文字表示系
			//「music」
			musicCharSp.Draw();
			//「曲名」
			titleCharSp.Draw();
			//「score」
			scoreCharSp.Draw();
			//「enemy」
			enemyCharSp.Draw();
			//数値表示系
			//ノート判定の数[判定4種][2桁]
			for(int i=0 ; i<TIMING_RANK_KIND ; i++)
			{
				for (int j=0 ; j<DIGIT ; j++)
				{
					noteRankNumSp[i,j].Draw();
				}
			}
			
			/*
			//コンボ数[桁数]
			for (int i = 0 ; i<DIGIT ; i++)
			{
				comboSp[i].Draw();	
			}
			*/
				
			//スコア[intの桁数]
			for (int i = 0 ; i<SCORE_DIGIT ; i++)
			{
				scoreSp[i].Draw();
			}
			
			//敵撃破数
			for (int i = 0; i<ENEMY_DIGIT ; i++)
			{
				enemySp[i].Draw();
			}
		}
		
		//整数を配列の各桁に変換する関数
		private void ArraySplit()
		{
			switch(oder)
			{
			//各判定の数
			case RESULT_ENUM.TIMINGNUM:
				for (int i=0 ; i<TIMING_RANK_KIND ; i++)
				{
					for (int j=0 ; j<DIGIT ; j++)
					{
						noteRankNumSp[i,j].FrameNo = (nowNoteRankNum[i] % (int)FMath.Pow(10.0f,j+1)) /  (int)FMath.Pow(10.0f,j);		
					}
				}
				break;
			
			//スコアの処理
			case RESULT_ENUM.SCORE:
				
				for(int i=0 ; i<SCORE_DIGIT ; i++)
				{
					scoreSp[i].FrameNo = (nowScore % (int)FMath.Pow(10.0f,i+1)) /  (int)FMath.Pow(10.0f,i);
				}
				break;
				
			//敵の撃破数とスコアの処理
			case RESULT_ENUM.ENEMY:
				
				for(int i=0 ; i<ENEMY_DIGIT ; i++)
				{
					enemySp[i].FrameNo = (nowEnemy % (int)FMath.Pow(10.0f,i+1)) /  (int)FMath.Pow(10.0f,i);
				}
				
				for(int i=0 ; i<SCORE_DIGIT ; i++)
				{
					scoreSp[i].FrameNo = (nowScore % (int)FMath.Pow(10.0f,i+1)) /  (int)FMath.Pow(10.0f,i);
				}
			break;
			}
		}
	}
}

