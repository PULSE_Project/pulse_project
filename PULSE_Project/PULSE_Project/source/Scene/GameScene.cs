//--------------------------------------------------------------
// クラス名： GameScene.cs
// コメント： ゲームシーンクラス
// 製作者　： ShionKubota
// 制作日時： 2014/02/28
// 更新日時： 2014/02/28
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Input;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.HighLevel.UI;

namespace PULSE_Project
{
	public class GameScene : BaseScene
	{
		public static PlayersUnit playersUnit;
		public static NoteManager noteManager;
		public static EnemyManager enemyManager;	// 敵管理
		public static GameUI gameUI;
		
		public static MusicId musicId = MusicId.Cyber06;
		
		public GameScene ()
		{
			Init ();
		}
		
		// 初期化
		override public void Init()
		{
			PlayData.GetInstance().Init();
			playersUnit = new PlayersUnit();
			noteManager = new NoteManager(musicId);
			enemyManager = new EnemyManager();
			gameUI = new GameUI();
		}
		
		// 更新
		override public void Update()
		{
			enemyManager.Update();
			playersUnit.Update();
			noteManager.Update();
			gameUI.Update();
			
			// 曲が終了したらゲーム終了
			if(noteManager.stopWatch.ElapsedMilliseconds > noteManager.notesData.length)
			{
				AppMain.sceneManager.Switch(SceneId.RESULT);
				noteManager.stopWatch.Stop();
				noteManager.stopWatch.Reset();
			}
			
			/*
			// デバッグ用 ゲームリトライ
			if(Input.GetInstance().start.isPushEnd)
			{
				AppMain.sceneManager.Switch(SceneId.SELECT);
				noteManager.stopWatch.Stop();
				noteManager.stopWatch.Reset();
			}
			
			// デバッグ用 リザルト移動用
			if(Input.GetInstance().triggerR.isPushEnd)
			{
				AppMain.sceneManager.Switch(SceneId.RESULT);
				noteManager.stopWatch.Stop();
				noteManager.stopWatch.Reset();
			}
			*/
			
			// 自機のライフが0になったらゲームオーバーへ行く処理
			if(playersUnit.life <= 0.0f)
			{
				AppMain.sceneManager.Switch(SceneId.GAMEOVER);
				noteManager.stopWatch.Stop();
				noteManager.stopWatch.Reset();
			}
		}
		
		// 描画
		override public void Draw()
		{
			background.Draw ();
			playersUnit.Draw();
			noteManager.Draw ();
			enemyManager.Draw();
			gameUI.Draw();
		}
		
		// 解放
		override public void Term()
		{
			playersUnit.Term();
			noteManager.Term();
			enemyManager.Term();
			gameUI.Term ();
		}
	}
}