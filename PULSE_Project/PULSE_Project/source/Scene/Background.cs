//--------------------------------------------------------------
// クラス名： Background.cs
// コメント： 背景クラス
// 製作者　： MasayukiTada
// 制作日時： 2014/03/26
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core ;
using Sce.PlayStation.Core.Graphics ;

namespace PULSE_Project
{
	public class Background
	{
		const int BACK_TEX_MAX = 8;
		
		// 背景
		private static Sprite2D[] sp = new Sprite2D[BACK_TEX_MAX];
		private static Texture2D[] tex = new Texture2D[BACK_TEX_MAX];
		
		// コンストラクタ
		public Background ()
		{
			if(tex[0] == null)
			{
				for(int i = 0; i < BACK_TEX_MAX;i++)
				{
					tex[i] = new Texture2D(@"Application/assets/img/back0" + i + ".png", false);
					sp[i] = new Sprite2D(tex[i]);
					sp[i].center = Vector2.Zero;
				}
			}
		}
		
		// 更新
		public void Update()
		{
			
		}
		
		// 描画
		public void Draw()
		{
			for(int i = BACK_TEX_MAX - 1; i >= 0;i--) sp[i].Draw();
		}
		
		// 解放
		public void Term()
		{
			for(int i = 0;i < BACK_TEX_MAX;i++)
			{
				sp[i].Term();
				tex[i].Dispose();
			}
		}
	}
}

