//--------------------------------------------------------------
// クラス名： GameOverScene.cs
// コメント： ゲームオーバーシーンクラス
// 製作者　： ShionKbota
// 制作日時： 2014/02/28
// 更新日時： 2014/02/28
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.Core.Input;
using Sce.PlayStation.HighLevel.UI;

namespace PULSE_Project
{
	public class GameOverScene : ResultScene
	{	
		Texture2D gameoverTex;
		Sprite2D gameoverSp;
		
		// コンストラクタ
		public GameOverScene ()
		{
			Init ();
		}
		
		// 初期化
		override public void Init()
		{
			gameoverTex = new Texture2D(@"/Application/assets/img/NotClear.png", false);
			gameoverSp = new Sprite2D(gameoverTex);
			gameoverSp.pos = AppMain.ScreenCenter;
		}
		
		// 更新
		override public void Update()
		{
			Input input = Input.GetInstance();
			if(input.circle.isPushEnd || input.cross.isPushEnd || input.triangle.isPushEnd ||
			   input.square.isPushEnd || input.triggerR.isPushEnd || input.triggerL.isPushEnd ||
			   input.start.isPushEnd || input.select.isPushEnd)
			{
				AppMain.sceneManager.Switch(SceneId.TITLE);
			}
		}
		
		// 解放
		override public void Term()
		{
			gameoverTex.Dispose();
			gameoverSp.Term();
		}
		
		// 描画
		override public void Draw()
		{
			background.Draw ();
			gameoverSp.Draw ();
		}
	}
}

