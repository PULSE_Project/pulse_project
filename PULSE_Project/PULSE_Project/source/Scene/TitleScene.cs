//--------------------------------------------------------------
// クラス名： TitleScene.cs
// コメント： タイトルシーンクラス
// 製作者　： ShionKubota
// 制作日時： 2014/02/28
// 更新日時： 2014/02/28
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.Core.Input;
using Sce.PlayStation.Core.Environment;
using System.Collections;
using System.Collections.Generic;
using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;

namespace PULSE_Project
{
	public class TitleScene : BaseScene
	{
		private Sprite2D logoSp;
		private Texture2D logoTex;
		
		private Sprite2D pressButtonSp;
		private Texture2D pushAnyBuutonTex;
		
		// 右下の円形ユニットとパルスリング
		private Sprite2D unitSp;
		private Texture2D unitTex;
		private List<Sprite2D> pulseSp;
		private Texture2D pulseTex;
		
		// BGM
		Music music;
		
		private int frameCnt;
		
		// コンストラクタ
		public TitleScene ()
		{
			Init ();
		}
		
		// 初期化
		override public void Init()
		{
			// ロゴ
			logoTex = new Texture2D(@"/Application/assets/img/TitleLogo.png", false);
			logoSp = new Sprite2D(logoTex);
			logoSp.pos = new Vector3(AppMain.ScreenWidth / 2.0f + 180.0f, AppMain.ScreenHeight / 2.1f, 0.0f);
			//logoSp.size = new Vector2(1.3f, 1.3f);
			logoSp.size = new Vector2(0.4f);
			
			// PressAnyButton
			pushAnyBuutonTex = new Texture2D(@"/Application/assets/img/PressAnyButton.png", false);
			pressButtonSp = new Sprite2D(pushAnyBuutonTex);
			pressButtonSp.pos = logoSp.pos;
			//pressButtonSp.pos.Y += 90.0f;
			pressButtonSp.pos.Y += 120.0f;
			pressButtonSp.size = new Vector2(0.5f, 0.5f);
			
			// 自キャラユニット
			unitTex = new Texture2D(@"/Application/assets/img/CentralUnit.png", false);
			unitSp = new Sprite2D(unitTex);
			unitSp.pos = new Vector3(0.0f, AppMain.ScreenHeight, 0.0f);
			
			// パルスリング
			pulseTex = new Texture2D(@"/Application/assets/img/PulseRing.png", false);
			pulseSp = new List<Sprite2D>();
			
			// BGM再生
			music = new Music(@"/Application/assets/music/burst.mp3");
			music.Set(true, 1.0f, 0.0f);
			
			this.fadeInTime = 120;
			this.fadeOutTime = 30;
			
			frameCnt = 0;
		}
		
		// 更新
		override public void Update()
		{
			// なにかボタンが押されたらフェードアウト開始
			Input input = Input.GetInstance();
			if(input.circle.isPushEnd || input.cross.isPushEnd || input.triangle.isPushEnd ||
			   input.square.isPushEnd || input.triggerR.isPushEnd || input.triggerL.isPushEnd ||
			   input.start.isPushEnd || input.select.isPushEnd)
			{
				AppMain.sceneManager.Switch(SceneId.SELECT);
				music.Stop();
			}
			
			// リングの生成
			GenRingByTiming(70);
			GenRingByTiming(230);
			GenRingByTiming(480);
			
			// 「PressAnyButton」点滅
			FlashingPAB();
			
			// リング管理処理
			ManageRing();
			
			frameCnt++;
		}
		
		// リングの生成管理
		private void GenRingByTiming(int timing)
		{
			if(frameCnt % timing == 0)
			{
				Sprite2D sp = new Sprite2D(pulseTex);
				sp.pos = new Vector3(0.0f, AppMain.ScreenHeight, 0.0f);
				sp.size = new Vector2(6.0f, 6.0f);
				sp.color = new Vector4(0.0f, 1.0f, 1.0f, 1.0f);
				pulseSp.Add(sp);
			}
		}

		// リング管理(移動から消滅まで)
		private void ManageRing()
		{
			// リングの移動
			float moveSpeed = 0.03f;
			for(int i = pulseSp.Count - 1; i >= 0; i--)
			{
				// 移動
				if(pulseSp[i].size.X > unitSp.size.X)
				{
					pulseSp[i].size -= new Vector2(moveSpeed, moveSpeed);
				}
				// 消滅
				else
				{
					if(pulseSp[i].color.W > 0.0f)
					{
						pulseSp[i].color.W -= 0.04f;
					}
					else
					{
						pulseSp.RemoveAt(i);
					}
				}
			}
		}
		
		// PressAnyButtonの点滅処理
		private void FlashingPAB()
		{
			float FLASHING_SLOWNESS = 10.0f;
			pressButtonSp.color.W = 0.5f + FMath.Sin (frameCnt / FLASHING_SLOWNESS) * 0.5f;
		}
		
		// 描画
		override public void Draw()
		{
			background.Draw ();
			pressButtonSp.Draw();
			unitSp.Draw();
			foreach(Sprite2D sp in pulseSp)
			{
				sp.Draw ();
			}
			logoSp.Draw ();
		}
		
		// 解放
		override public void Term()
		{
			logoTex.Dispose();
			pushAnyBuutonTex.Dispose();
			unitTex.Dispose();
			pulseTex.Dispose();
			music.Term();
			for(int i = pulseSp.Count - 1; i >= 0; i--)
			{
				pulseSp.RemoveAt(i);
			}
		}
	}
}

