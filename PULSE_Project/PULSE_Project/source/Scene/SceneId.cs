//--------------------------------------------------------------
// クラス名： SceneState.cs
// コメント： シーン番号定数
// 製作者　： ShionKubota
// 制作日時： 2014/02/28
//--------------------------------------------------------------
using System;

namespace PULSE_Project
{
	public enum SceneId
	{
		LOGO,
		TITLE,
		SELECT,
		GAME,
		RESULT,
		GAMEOVER
	}
}

