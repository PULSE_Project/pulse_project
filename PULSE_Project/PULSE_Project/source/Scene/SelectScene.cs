//--------------------------------------------------------------
// クラス名： SelectScene.cs
// コメント： 楽曲セレクトシーン
// 製作者　： ShionKubota
// 制作日時： 2014/03/06
// 更新日時： 2014/03/06
//--------------------------------------------------------------
using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

namespace PULSE_Project
{
	public class SelectScene : BaseScene
	{
		private const int MUSIC_NUM = 5;
		private const int RING_NUM = 5;
		private const int SELECTABLE_MUSIC_NUM = 4;	// 現在選択可能な曲の数
		private const int SELECT_POS_NO = 2;		// このポジションNoに位置するリングを選択対象とする
		
		// 曲のタイトルが乗っかるリング
		private struct TitleRing
		{
			public Sprite2D ringSp;
			public Sprite2D titleSp;
			public int posNo;			// リングの,場所ごとの番号.(左から0, 1, 2...)
			public MusicId titleId;
		}
		private TitleRing[] ring = new TitleRing[RING_NUM];
		private Texture2D ringTex;
		private float moveSlowness;	// リング移動の遅さ
		private bool isMoving;
		private bool isSelected;
		
		// 楽曲のタイトル画像
		private Texture2D titleTex;
		
		// 楽曲
		Music[] music = new Music[SELECTABLE_MUSIC_NUM];
		
		public SelectScene ()
		{
			Init ();
		}
		
		// 初期化
		public override void Init()
		{
			ringTex = new Texture2D(@"/Application/assets/img/MTitlePulse.png", false);
			titleTex = new Texture2D(@"/Application/assets/img/MTitle.png", false);
			for(int i = 0; i < RING_NUM; i++)
			{
				// リング
				ring[i].ringSp = new Sprite2D(ringTex);
				ring[i].ringSp.size = new Vector2(0.0f, 0.0f);
				ring[i].posNo = i;
				
				// タイトル文字
				ring[i].titleSp = new Sprite2D(titleTex);
				ring[i].titleSp.textureUV = new Vector4(0.0f, 0.0f, 1.0f / 3.0f, 1.0f / 3.0f);
				
				// 楽曲ID
				int titleId = (i - SELECT_POS_NO);
				if(titleId < 0) titleId = MUSIC_NUM + titleId;	// IDマイナス防止
				ring[i].titleId = (MusicId)titleId;
				
				// タイトル画像の切り取り矩形を変更
				ring[i].titleSp.textureUV = GetTitleTexUV(ring[i].titleId);
			}
			
			// 楽曲読み込み
			for(int i = 0; i < SELECTABLE_MUSIC_NUM; i++)
			{
				music[i] = new Music(@"/Application/assets/music/" + 
			    	              NotesDataCollection.GetInstance()[i].fileName);
			}
			
			// 楽曲を再生
			PlayToSelectingMusic();
			
			this.fadeInTime = 10;
			this.fadeOutTime = 10;
			
			moveSlowness = 3.0f;
			isMoving = true;
			isSelected = false;
		}
		
		// 更新
		public override void Update()
		{
			ManageInput();
			
			// 移動中処理
			if(isMoving)
			{
				MoveRingProcess();
			}
			
			// 決定後の演出
			if(isSelected)
			{
				SelectedProcess();
			}
		}
		
		// プレイヤーの入力を検地、処理---------------------------------------------------
		private void ManageInput()
		{
			Input input = Input.GetInstance();
			
			// 移動ボタン押されたら
			if(!isMoving && !isSelected && (input.right.isPush || input.left.isPush))
			{
				isMoving = true;
				
				// 左
				if(input.left.isPush)
				{	
					for(int i = 0; i < RING_NUM; i++)
					{
						if(++ring[i].posNo >= RING_NUM) ring[i].posNo = 0;
					}
					ring[0].titleId = (MusicId)((int)ring[1].titleId - 1);
					if(ring[0].titleId < (MusicId)0) ring[0].titleId = (MusicId)(MUSIC_NUM - 1);
				}
				
				// 右
				if(input.right.isPush)
				{
					for(int i = 0; i < RING_NUM; i++)
					{
						if(--ring[i].posNo < 0) ring[i].posNo = RING_NUM - 1;
					}
					ring[RING_NUM - 1].titleId = (MusicId)((int)ring[RING_NUM - 2].titleId + 1);
					if(ring[0].titleId < (MusicId)0) ring[0].titleId = (MusicId)(MUSIC_NUM - 1);
				}
				
				
				for(int i = 0; i < RING_NUM; i++)
				{
					// タイトル画像の切り取り矩形を変更
					ring[i].titleSp.textureUV = GetTitleTexUV(ring[i].titleId);
				}
			}
			
			// 決定ボタン押されたら
			if(input.circle.isPushStart && !isMoving)
			{
				for(int i = 0; i < RING_NUM; i++)
				{
					if(ring[i].posNo != SELECT_POS_NO) continue;
					if(ring[i].titleId >= (MusicId)SELECTABLE_MUSIC_NUM)	break;
					
					GameScene.musicId = ring[i].titleId;
					isSelected = true;
				}
			}
		}
		
		// リング移動処理----------------------------------------------
		private void MoveRingProcess()
		{
			float[] sizeTable = new float[]{0.4f, 0.7f, 1.0f, 1.35f, 1.7f};	// 各リングのサイズを保存
			bool isAllStoped = true;
			for(int i = 0; i < RING_NUM; i++)
			{
				// 移動
				float STOP_AREA = 0.003f;	// 目標との差がこの数値以下になったらストップ
				if(Math.Abs (ring[i].ringSp.size.X - sizeTable[ring[i].posNo]) > STOP_AREA)
				{
					// 目的サイズと現在サイズの差分を一定の数値で割り、足していく
					float newSize = (sizeTable[ring[i].posNo] - ring[i].ringSp.size.X) / moveSlowness;
					ring[i].ringSp.size += new Vector2(newSize, newSize);
					isAllStoped = false;
				}
				// 停止
				else
				{
					ring[i].ringSp.size = new Vector2(sizeTable[ring[i].posNo], sizeTable[ring[i].posNo]);
					
					// 一定時間以上ボタンを押していたら、移動スピードを上げる
					double decisionHoldSec = 0.5f;	// この秒数以上おしていたら
					if(Input.GetInstance().right.holdingSeconds > decisionHoldSec ||
					   Input.GetInstance().left.holdingSeconds > decisionHoldSec)
					{
						moveSlowness = 1.6f;
					}
					// ボタンが離されていたらスピードを元に戻し、選ばれている楽曲を再生
					else
					{
						moveSlowness = 3.0f;
						// 楽曲を再生
						PlayToSelectingMusic();
					}
				}
				
				// 透明度変更
				float END_AREA = 0.01f;			// 目標との差がこの数値以内ならそこでストップ
				float CHANGE_SLOWNESS = 5.0f;	// 変化の遅さ
				float endAlpha = 1.0f - (float)Math.Abs((SELECT_POS_NO - ring[i].posNo)) * 0.5f;
				if(Math.Abs (ring[i].ringSp.color.W - endAlpha) > END_AREA)
				{
					float newAlpha = (endAlpha - ring[i].ringSp.color.W) / CHANGE_SLOWNESS;
					ring[i].ringSp.color.W += newAlpha;
				}
				
				// 色変更
				if((int)ring[i].titleId >= SELECTABLE_MUSIC_NUM)
				{
					// 赤
					ring[i].ringSp.color.X = 1.0f;
					ring[i].ringSp.color.Y = 0.0f;
					ring[i].ringSp.color.Z = 0.0f;
				}
				else
				{
					// 水色
					ring[i].ringSp.color.X = 0.3f;
					ring[i].ringSp.color.Y = 1.0f;
					ring[i].ringSp.color.Z = 1.0f;
				}
				
				// サイズに合わせて移動
				MoveRingBySize(ring[i].ringSp);
				
				// タイトル文字をリングに同期
				ring[i].titleSp.pos = ring[i].ringSp.pos;
				ring[i].titleSp.size = ring[i].ringSp.size * 0.5f;
				ring[i].titleSp.color = ring[i].ringSp.color;
			}
			if(isAllStoped) isMoving = false;
		}
		
		// 楽曲決定後の処理------------------------------------
		private void SelectedProcess()
		{
			for(int i = 0; i < RING_NUM; i++)
			{
				// 選ばれて無いやつ
				if(ring[i].posNo != SELECT_POS_NO)
				{
					ring[i].ringSp.color.W -= 0.01f;
				}
				
				// 選ばれたやつ
				else
				{
					if(ring[i].ringSp.size.X < 3.0f)
					{
						ring[i].ringSp.size += new Vector2(0.06f, 0.06f);
						MoveRingBySize(ring[i].ringSp);
					}
					else
					{
						AppMain.sceneManager.Switch(SceneId.GAME);
					}
				}
				
				// タイトル文字をフェードアウト
				ring[i].titleSp.color.W -= 0.05f;
			}
		}
		
		// 現在サイズに合わせて移動
		private void MoveRingBySize(Sprite2D ring)
		{
			ring.Draw ();
			ring.pos.X = ring.width / 2.0f;
			ring.pos.Y = AppMain.ScreenHeight - ring.height / 2.0f;
		}
		
		// 選択中の楽曲を再生
		private void PlayToSelectingMusic()
		{
			// 音楽を再生
			for(int i = 0; i < RING_NUM; i++)
			{
				if(ring[i].posNo != SELECT_POS_NO) continue;
				
				int musicNo = (int)ring[i].titleId;
				
				// 楽曲を停止
				try{ Music.player.Stop(); }
				catch(ObjectDisposedException e)
				{
					Console.WriteLine("stopでexception");
				}
				
				// COMING SOON の曲の場合は再生しない
				if((int)ring[i].titleId >= SELECTABLE_MUSIC_NUM)
				{
					break;
				}
				
				// 再生
				music[musicNo].Set(true, 1.0f, 6.0f);
				break;
			}
		}
		
		// 楽曲IDに合わせてタイトル画像のUV値を変更
		private Vector4 GetTitleTexUV(MusicId id)
		{
			float len = 1.0f / 3.0f;
			int[] frame = new int[2];
			
			Vector4 uv;
			switch(id)
			{
			case MusicId.Cyber06:
				frame = new int[]{0, 0};
				break;
				
			case MusicId.SIXTH_SENSE:
				frame = new int[]{1, 0};
				break;
				
			case MusicId.PowerForTommorow:
				frame = new int[]{2, 0};
				break;
				
			case MusicId.Celluloid_Splash:
				frame = new int[]{0, 1};
				break;
				
			default:
				frame = new int[]{2, 2};
				break;
			}
			
			uv = new Vector4(frame[0] * len, frame[1] * len, frame[0] * len + len, frame[1] * len + len);
			
			return uv;
		}
		
		// 描画
		public override void Draw()
		{
			background.Draw ();
			for(int i = 0; i < RING_NUM; i++)
			{
				ring[i].ringSp.Draw();
				ring[i].titleSp.Draw();
			}
		}
		
		// 解放
		public override void Term()
		{
			for(int i = 0; i < RING_NUM; i++)
			{
				ring[i].ringSp.Term();
				ring[i].titleSp.Term();
			}
			
			// 音楽を全停止して解放
			foreach(Music m in music)
			{
				m.Stop();
			}
			foreach(Music m in music)
			{
				m.Term();
			}
			ringTex.Dispose();
			titleTex.Dispose();
		}
	}
}

