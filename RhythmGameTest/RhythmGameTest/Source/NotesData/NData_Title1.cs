using System;

namespace RhythmGameTest
{
	public class NData_Title1 : NotesData
	{	
		public NData_Title1 ()
		{
			fileName = "Bgm_Game.mp3";
			id = MusicId.title1;
			
			// 譜面データ
			AddNote (1016L);
			AddNote (1616L);
			AddNote (2215L);
			AddNote (2518L);
			AddNote (2855L);
			AddNote (3005L);
			AddNote (3155L);
			AddNote (3305L);
			AddNote (3300L, 4330L);
		}
		
		private void Init()
		{
			
		}
	}
}

