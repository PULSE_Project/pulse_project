using System;
using Sce.PlayStation.Core;

namespace RhythmGameTest
{
	public class Note
	{
		public long timing{ get; private set; }
		public long endTiming{ get; private set; }
		public bool isLongNote{ get; private set; }
		public bool isClear;
		
		// 通常ノート
		public Note(long timing)
		{
			this.timing = timing;
			isLongNote = false;
			isClear = false;
		}
		
		// ロングノート
		public Note(long timing, long endTiming)
		{
			this.timing = timing;
			this.endTiming = endTiming;
			isLongNote = true;
			isClear = false;
		}
	}
}

