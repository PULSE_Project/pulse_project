using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;

namespace RhythmGameTest
{
	public class NotesData
	{
		// 楽曲のタイトル
		public string fileName{ get; protected set; }
		public MusicId id{ get; protected set; }
		
		// 譜面データ
		public List<Note> notes{ get; protected set; }
		/*
		protected Note[] notes;
		public Note this[int index]
		{
			get
			{
				return notes[index];
			}
			
			protected set
			{
				notes[index] = value;
			}
		}
		*/
		
		public NotesData()
		{
			notes = new List<Note>();
		}
		
		// ノートセット
		protected void AddNote(long timing)
		{
			notes.Add(new Note(timing));
		}
		// ロングノートセット
		protected void AddNote(long timing, long endTiming)
		{
			notes.Add(new Note(timing, endTiming));
		}
	}
}

