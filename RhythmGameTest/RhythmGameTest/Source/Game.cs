using System;
using System.Diagnostics ;
using Sce.PlayStation.Core;

namespace RhythmGameTest
{
	public class Game
	{
		private const long PERFECT_ZONE = 10L;
		Stopwatch stopwatch;
		NotesData notesData;
		Music music;
		
		public Game()
		{
			stopwatch = new Stopwatch();
			stopwatch.Start();
			notesData = new NData_Title1();
			music = new Music(@"/Application/assets/sound/" + notesData.fileName);
			music.Set(false, 1.0f);
			
			Console.WriteLine ("MUSIC S.T.A.R.T!");
		}
		
		public void Update()
		{
			long nowTime = stopwatch.ElapsedMilliseconds;
			
			foreach(Note note in notesData.notes)
			{
				if(note.isClear) continue;
				
				// ロングノートのチェック
				if(note.isLongNote)
				{
					if(note.timing < nowTime && note.endTiming > nowTime)
					{
						Console.WriteLine("stopwatch = " + stopwatch.ElapsedMilliseconds);
					}
					
					if(note.endTiming < nowTime)
					{
						note.isClear = true;
					}
				}
				
				// 通常ノートのチェック
				else if(FMath.Abs(nowTime - note.timing) < PERFECT_ZONE)
				{
					Console.WriteLine("stopwatch = " + stopwatch.ElapsedMilliseconds);
					
					note.isClear = true;
				}
			}
		}
		
		public void Draw()
		{
		}
		
		public void Term()
		{
		}
	}
}

